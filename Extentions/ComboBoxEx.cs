﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS_RFID3_Host_Sample2.Extentions
{
    public class ComboBoxEx
    {
        public string Text { get; set; }
        public long Value { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }
}
