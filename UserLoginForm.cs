﻿using CS_RFID3_Host_Sample2.RESTFulAPI.Constant;
using CS_RFID3_Host_Sample2.RESTFulAPI.Model;
using CS_RFID3_Host_Sample2.RESTFulAPI.RequestHandler;
using CS_RFID3_Host_Sample2.Static;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CS_RFID3_Host_Sample2
{
    public partial class UserLoginForm : Form
    {
        public UserLoginForm()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            InterfaceRequestHandlers webClientRequestHandlers = new WebClientRequestHandler();

            if (txtUsername.Text == "" || txtPassword.Text == "")
            {
                MessageBox.Show("Mohon isi UserName dan Password");
                return;
            }

            try
            {
                Console.WriteLine("Start getting user...");
                var response = GetUser(webClientRequestHandlers, txtUsername.Text, txtPassword.Text);

                Console.WriteLine("Set response to object USER...");
                var user = JsonConvert.DeserializeObject<ResponseJson>(response);

                //to do
                if(user.Status == "true")
                {
                    //MessageBox.Show("Login Berhasil", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.alertNotif("Login Berhasil", AlertForm.enmType.Success);
                    string respData = user.ResponseData.ToString();

                    JObject jObj = JObject.FromObject(user.ResponseData);

                    SetUserValue(jObj);

                    Console.WriteLine("Api Token : " + UserStatic.userToken);

                    this.Hide();
                    AppForm af = new AppForm();
                    af.Show();
                }
                else
                {
                    //MessageBox.Show("Login Gagal!", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.alertNotif(user.ResponseMsg, AlertForm.enmType.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
        }


        //set static value for Login User
        private static void SetUserValue(JObject jObj)
        {
            UserStatic.userId = (long)jObj.SelectToken("id");
            UserStatic.userName = (string)jObj.SelectToken("username");
            UserStatic.userToken = (string)jObj.SelectToken("api_token");
            UserStatic.userRealName = (string)jObj.SelectToken("name");
            UserStatic.groupUser = (string)jObj.SelectToken("group_user");

            var dtNow = DateTime.Now;
            var dtNowOffset = new DateTimeOffset(dtNow).ToUniversalTime();
            long millSecTime = (Int64)(dtNowOffset.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
            UserStatic.loginTimeStamp = millSecTime.ToString();         //DateTime.Now.ToString("yyyyMMddHHmmssffff");
        }

        //end of set static value for Login User


        public static string GetUser(InterfaceRequestHandlers requestHandlers, string username, string password)
        {
            string url = RequestConstant.BaseUrl + "login";
           
            Console.WriteLine("Hit to : " + url);
            return requestHandlers.GetUser(url, username, password);
        }

        private void loginPanel_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void UserLoginForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        public void alertNotif(string msg, AlertForm.enmType type)
        {
            AlertForm af = new AlertForm();
            af.showAlert(msg, type);
        }
    }
}
