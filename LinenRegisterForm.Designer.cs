﻿
namespace CS_RFID3_Host_Sample2
{
    partial class LinenRegisterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbRuangan = new System.Windows.Forms.ComboBox();
            this.cbNamaCustomer = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cbNamaLinen = new System.Windows.Forms.ComboBox();
            this.cbStatusLinen = new System.Windows.Forms.ComboBox();
            this.txtRFID = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnRead = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnDeleteItem = new System.Windows.Forms.Button();
            this.listRegisteredLinen = new System.Windows.Forms.ListView();
            this.columnRFID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnLinenName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnCustomer = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnRoom = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnTglReg = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnRegBy = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listViewPrepareRegister = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.cbKebutuhan = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cbRuangan);
            this.panel1.Controls.Add(this.cbNamaCustomer);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(12, 53);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(482, 110);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // cbRuangan
            // 
            this.cbRuangan.FormattingEnabled = true;
            this.cbRuangan.Location = new System.Drawing.Point(158, 73);
            this.cbRuangan.Name = "cbRuangan";
            this.cbRuangan.Size = new System.Drawing.Size(261, 21);
            this.cbRuangan.TabIndex = 6;
            // 
            // cbNamaCustomer
            // 
            this.cbNamaCustomer.FormattingEnabled = true;
            this.cbNamaCustomer.Location = new System.Drawing.Point(158, 42);
            this.cbNamaCustomer.Name = "cbNamaCustomer";
            this.cbNamaCustomer.Size = new System.Drawing.Size(261, 21);
            this.cbNamaCustomer.TabIndex = 5;
            this.cbNamaCustomer.SelectedIndexChanged += new System.EventHandler(this.cbNamaCustomer_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Nama Customer :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Ruangan";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Customer : ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(178, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Form Registrasi";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cbKebutuhan);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.cbNamaLinen);
            this.panel2.Controls.Add(this.cbStatusLinen);
            this.panel2.Controls.Add(this.txtRFID);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.btnRead);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Location = new System.Drawing.Point(12, 169);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(584, 178);
            this.panel2.TabIndex = 5;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // cbNamaLinen
            // 
            this.cbNamaLinen.FormattingEnabled = true;
            this.cbNamaLinen.Location = new System.Drawing.Point(158, 42);
            this.cbNamaLinen.Name = "cbNamaLinen";
            this.cbNamaLinen.Size = new System.Drawing.Size(261, 21);
            this.cbNamaLinen.TabIndex = 11;
            // 
            // cbStatusLinen
            // 
            this.cbStatusLinen.FormattingEnabled = true;
            this.cbStatusLinen.Location = new System.Drawing.Point(158, 73);
            this.cbStatusLinen.Name = "cbStatusLinen";
            this.cbStatusLinen.Size = new System.Drawing.Size(261, 21);
            this.cbStatusLinen.TabIndex = 10;
            this.cbStatusLinen.SelectedIndexChanged += new System.EventHandler(this.cbStatusLinen_SelectedIndexChanged);
            // 
            // txtRFID
            // 
            this.txtRFID.Location = new System.Drawing.Point(158, 139);
            this.txtRFID.Name = "txtRFID";
            this.txtRFID.Size = new System.Drawing.Size(261, 20);
            this.txtRFID.TabIndex = 9;
            this.txtRFID.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 139);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "No. Seri RFID :";
            // 
            // btnRead
            // 
            this.btnRead.Location = new System.Drawing.Point(468, 139);
            this.btnRead.Name = "btnRead";
            this.btnRead.Size = new System.Drawing.Size(75, 25);
            this.btnRead.TabIndex = 5;
            this.btnRead.Text = "Read";
            this.btnRead.UseVisualStyleBackColor = true;
            this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Nama Linen :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Status Linen : ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Linen : ";
            // 
            // btnDeleteItem
            // 
            this.btnDeleteItem.Location = new System.Drawing.Point(632, 446);
            this.btnDeleteItem.Name = "btnDeleteItem";
            this.btnDeleteItem.Size = new System.Drawing.Size(75, 25);
            this.btnDeleteItem.TabIndex = 6;
            this.btnDeleteItem.Text = "Delete Item";
            this.btnDeleteItem.UseVisualStyleBackColor = true;
            this.btnDeleteItem.Click += new System.EventHandler(this.btnDeleteItem_Click);
            // 
            // listRegisteredLinen
            // 
            this.listRegisteredLinen.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnRFID,
            this.columnLinenName,
            this.columnCustomer,
            this.columnRoom,
            this.columnTglReg,
            this.columnRegBy,
            this.columnStatus});
            this.listRegisteredLinen.HideSelection = false;
            this.listRegisteredLinen.Location = new System.Drawing.Point(12, 542);
            this.listRegisteredLinen.Name = "listRegisteredLinen";
            this.listRegisteredLinen.Size = new System.Drawing.Size(746, 136);
            this.listRegisteredLinen.TabIndex = 6;
            this.listRegisteredLinen.UseCompatibleStateImageBehavior = false;
            this.listRegisteredLinen.View = System.Windows.Forms.View.Details;
            // 
            // columnRFID
            // 
            this.columnRFID.Text = "No. Seri RFID";
            this.columnRFID.Width = 70;
            // 
            // columnLinenName
            // 
            this.columnLinenName.Text = "Nama Linen";
            this.columnLinenName.Width = 95;
            // 
            // columnCustomer
            // 
            this.columnCustomer.Text = "Nama Customer";
            this.columnCustomer.Width = 119;
            // 
            // columnRoom
            // 
            this.columnRoom.Text = "Ruangan";
            this.columnRoom.Width = 69;
            // 
            // columnTglReg
            // 
            this.columnTglReg.Text = "Tanggal Registrasi";
            this.columnTglReg.Width = 111;
            // 
            // columnRegBy
            // 
            this.columnRegBy.Text = "Registrasi Oleh";
            this.columnRegBy.Width = 145;
            // 
            // columnStatus
            // 
            this.columnStatus.Text = "Status Linen";
            this.columnStatus.Width = 116;
            // 
            // listViewPrepareRegister
            // 
            this.listViewPrepareRegister.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader7,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader8});
            this.listViewPrepareRegister.GridLines = true;
            this.listViewPrepareRegister.HideSelection = false;
            this.listViewPrepareRegister.Location = new System.Drawing.Point(12, 366);
            this.listViewPrepareRegister.Name = "listViewPrepareRegister";
            this.listViewPrepareRegister.Size = new System.Drawing.Size(584, 136);
            this.listViewPrepareRegister.TabIndex = 7;
            this.listViewPrepareRegister.UseCompatibleStateImageBehavior = false;
            this.listViewPrepareRegister.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "No. Seri RFID";
            this.columnHeader1.Width = 125;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Nama Linen";
            this.columnHeader2.Width = 163;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Nama Customer";
            this.columnHeader3.Width = 138;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Ruangan";
            this.columnHeader4.Width = 69;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Status Linen";
            this.columnHeader7.Width = 105;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "locId";
            this.columnHeader5.Width = 0;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "customerId";
            this.columnHeader6.Width = 0;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "linenId";
            this.columnHeader8.Width = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 350);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(156, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "List Linen yang akan diregister :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(25, 526);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(136, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "List Linen Baru Teregister : ";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(632, 384);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 25);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cbKebutuhan
            // 
            this.cbKebutuhan.FormattingEnabled = true;
            this.cbKebutuhan.Location = new System.Drawing.Point(158, 105);
            this.cbKebutuhan.Name = "cbKebutuhan";
            this.cbKebutuhan.Size = new System.Drawing.Size(261, 21);
            this.cbKebutuhan.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 105);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 12;
            this.label11.Text = "Kebutuhan : ";
            // 
            // LinenRegisterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(770, 690);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.listViewPrepareRegister);
            this.Controls.Add(this.btnDeleteItem);
            this.Controls.Add(this.listRegisteredLinen);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "LinenRegisterForm";
            this.Text = "LinenRegisterForm";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnDeleteItem;
        private System.Windows.Forms.Button btnRead;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtRFID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbRuangan;
        private System.Windows.Forms.ComboBox cbNamaCustomer;
        private System.Windows.Forms.ComboBox cbNamaLinen;
        private System.Windows.Forms.ComboBox cbStatusLinen;
        private System.Windows.Forms.ListView listRegisteredLinen;
        private System.Windows.Forms.ColumnHeader columnRFID;
        private System.Windows.Forms.ColumnHeader columnLinenName;
        private System.Windows.Forms.ColumnHeader columnCustomer;
        private System.Windows.Forms.ColumnHeader columnRoom;
        private System.Windows.Forms.ColumnHeader columnTglReg;
        private System.Windows.Forms.ColumnHeader columnRegBy;
        private System.Windows.Forms.ColumnHeader columnStatus;
        private System.Windows.Forms.ListView listViewPrepareRegister;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ComboBox cbKebutuhan;
        private System.Windows.Forms.Label label11;
    }
}