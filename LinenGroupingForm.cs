﻿using CS_RFID3_Host_Sample2.RESTFulAPI.Constant;
using CS_RFID3_Host_Sample2.RESTFulAPI.Model;
using CS_RFID3_Host_Sample2.RESTFulAPI.RequestHandler;
using CS_RFID3_Host_Sample2.Static;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Symbol.RFID3;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CS_RFID3_Host_Sample2
{
    public partial class LinenGroupingForm : Form
    {
        private RFIDReader readerAPI;
        private PostFilterForm pff;
        private TriggersForm tf;
        private AntennaInfoForm aif;
        private AccessFilterForm aff;
        private TagData[] readTag;

        private bool isConnected;

        public LinenGroupingForm(RFIDReader m_ReaderAPI, bool m_IsConnected, PostFilterForm m_PostFilterForm, TriggersForm m_TriggerForm, AntennaInfoForm m_AntennaInfoForm, AccessFilterForm m_AccessFilterForm)
        {
            InitializeComponent();

            this.readerAPI = m_ReaderAPI;
            this.isConnected = m_IsConnected;
            this.pff = m_PostFilterForm;
            this.tf = m_TriggerForm;
            this.aif = m_AntennaInfoForm;
            this.aff = m_AccessFilterForm;


        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            try
            {

                if (isConnected)
                {
                    if (btnRead.Text == "Read")
                    {
                        bool scannerRunning = true;
                        btnRead.Text = "Stop";
                        if (readerAPI.Actions.TagAccess.OperationSequence.Length > 0)
                        {
                            readerAPI.Actions.TagAccess.OperationSequence.PerformSequence(aff.getFilter(),
                                tf.getTriggerInfo(), aif.getInfo());
                        }
                        else
                        {
                            readerAPI.Actions.Inventory.Perform(
                              pff.getFilter(),
                              tf.getTriggerInfo(),
                              aif.getInfo()
                           );
                        }

                        Console.WriteLine("Get Read Tags...");
                        Symbol.RFID3.TagData[] tagDatas = readerAPI.Actions.GetReadTags(1000);
                        if (null != tagDatas)
                        {
                            foreach(TagData td in tagDatas)
                            {
                                string tagID = td.TagID;
                                string antennaId = td.AntennaID.ToString();
                                Console.WriteLine("Tag : " + td.TagID + " from Antenna : "+antennaId);

                                /*
                                 * check Existing Linen by RFID
                                 */
                                if (tagID == "")
                                {
                                    MessageBox.Show("Error : No RFID Detected.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }
                                else
                                {
                                    
                                    JObject linenIsExist = LinenIsExist(tagID);
                                    int total = (int)linenIsExist.SelectToken("total");
                                    if (total > 0)
                                    {
                                        Console.WriteLine("RFID " + tagID + " exists . . .");
                                        JArray arrLinen = (JArray)linenIsExist.SelectToken("data");
                                        JObject linenObj = JObject.FromObject(arrLinen[0]);

                                        string namaCustomer = (string)linenObj.SelectToken("company_name");
                                        string ruangan = (string)linenObj.SelectToken("location_name");
                                        string namaLinen = (string)linenObj.SelectToken("item_product_name");
                                        string statusLinen = (string)linenObj.SelectToken("item_linen_rent");

                                        if(antennaId == "1")
                                        {
                                            lblRFID1.Text = tagID;
                                            lblCustomer1.Text = namaCustomer;
                                            lblLinen1.Text = namaLinen;
                                            lblRuangan1.Text = ruangan;

                                            lblRFID1.Visible = true;
                                            lblCustomer1.Visible = true;
                                            lblLinen1.Visible = true;
                                            lblRuangan1.Visible = true;
                                        }
                                        else
                                        {
                                            lblRFID2.Text = tagID;
                                            lblCustomer2.Text = namaCustomer;
                                            lblLinen2.Text = namaLinen;
                                            lblRuangan2.Text = ruangan;

                                            lblRFID2.Visible = true;
                                            lblCustomer2.Visible = true;
                                            lblLinen2.Visible = true;
                                            lblRuangan2.Visible = true;
                                        }
                                    }
                                    else
                                    {
                                        /*SetLinenParams(out locationId, out productId, out companyId, out rfid, out rent, out body, isUpdate);
                                        var response = RegisterLinen(body, isUpdate);
                                        GetResultOfSaveOrUpdate(response);

                                        LoadNewRegisteredLinen();*/
                                        MessageBox.Show("Error : RFID is not registered.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        readerAPI.Actions.Inventory.Stop();
                                        scannerRunning = false;
                                        break;
                                    }


                                }


                            }
                        }

                        if(!scannerRunning)
                        btnRead.Text = "Read";
                    }
                    else if (btnRead.Text == "Stop")
                    {
                        readerAPI.Actions.Inventory.Stop();

                        btnRead.Text = "Read";
                        //btnUpdate.Enabled = true;
                    }
                }

            }
            catch (InvalidOperationException ioe)
            {
                MessageBox.Show("Error : " + ioe.Message, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (InvalidUsageException iue)
            {
                MessageBox.Show("Error : " + iue.Message, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (OperationFailureException ofe)
            {
                MessageBox.Show("Error : " + ofe.Message, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private static JObject LinenIsExist(string rfid)
        {
            //int total = 0;
            JObject jObj = new JObject();
            try
            {
                InterfaceRequestHandlers webRequestHandlers = new WebClientRequestHandler();
                var response = CheckLinen(webRequestHandlers, UserStatic.userToken, rfid);
                var linen = JsonConvert.DeserializeObject<ResponseJson>(response);
                if (linen.Status == "true")
                {
                    jObj = JObject.FromObject(linen.ResponseData);
                    //total = (int)jObj.SelectToken("total");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
            return jObj;
        }

        private static string CheckLinen(InterfaceRequestHandlers webRequestHandlers, string token, string rfid)
        {
            string url = RequestConstant.BaseUrl + RequestConstant.LinenPath + RequestConstant.GetData;
            PostDataParams pdp = new PostDataParams
            {
                search = rfid,
            };
            string body = JsonConvert.SerializeObject(pdp);

            Console.WriteLine("Hit url : " + url);
            Console.WriteLine("Body : " + body);
            return webRequestHandlers.RequestApi(url, token, body);
        }
    }
}
