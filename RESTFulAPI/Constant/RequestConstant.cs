﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS_RFID3_Host_Sample2.RESTFulAPI.Constant
{
    class RequestConstant
    {
        public const string BaseUrl = "https://dev.obsesiman.com/api/";
        
        public const string UserAgent = "User-Agent";
        public const string UserAgentValue = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36";

        public const string Accept = "Accept";
        public const string AcceptValue = "application/json";

        public const string ContentType = "Content-Type";
        public const string ContentTypeValue = "application/json";

        public const string Authorization = "Authorization";
        public const string AuthorizationValue = "Bearer ";

        //API CRUD
        public const string GetData = "/data";
        public const string DeleteData = "/delete";
        public const string PatchData = "/patch";
        public const string SaveData = "/create";
        public const string UpdateData = "/update";

        //API Path
        public const string LoginPath = "login";
        public const string CustomerPath = "system_company";
        public const string LinenPath = "item_linen";
        //public const string LinenStatusPath = "login";
        //public const string LoginPath = "login";


    }
}
