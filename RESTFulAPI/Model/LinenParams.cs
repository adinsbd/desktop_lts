﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS_RFID3_Host_Sample2.RESTFulAPI.Model
{
    class LinenParams
    {
        public long item_linen_location_id;
        public long item_linen_company_id;
        public long item_linen_product_id;
        public long item_linen_rent;
        public long item_linen_status;
        public string item_linen_rfid;
        public string item_linen_session;
    }
}
