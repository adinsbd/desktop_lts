﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS_RFID3_Host_Sample2.RESTFulAPI.Model
{
    public class Customer
    {
        [JsonProperty(PropertyName = "company_id")]
        public long CompanyId { get; set; }

        [JsonProperty(PropertyName = "company_name")]
        public string CompanyName { get; set; }

        [JsonProperty(PropertyName = "locations")]
        public JArray Locations { get; set; }

        [JsonProperty(PropertyName = "products")]
        public JArray Products { get; set; }
    }

    /*class Location
    {
        [JsonProperty(PropertyName = "location_id")]
        public long LocationId { get; set; }

        [JsonProperty(PropertyName = "location_name")]
        public string LocationName { get; set; }
    }

    class Product
    {
        [JsonProperty(PropertyName = "item_product_id")]
        public long ProductId { get; set; }

        [JsonProperty(PropertyName = "item_product_name")]
        public string ProductName { get; set; }
    }*/
}
