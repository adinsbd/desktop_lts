﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS_RFID3_Host_Sample2.RESTFulAPI.Model
{
    class PostDataParams
    {
        public string search { get; set; }

        public string code { get; set; }

        public string aggregate { get; set; }

        public int page { get; set; }

        public int limit { get; set; }
    }
}
