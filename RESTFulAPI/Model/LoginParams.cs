﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS_RFID3_Host_Sample2.RESTFulAPI.Model
{
    class LoginParams
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}
