﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS_RFID3_Host_Sample2.RESTFulAPI.Model
{
    class ResponseJson
    {
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
        
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [JsonProperty(PropertyName = "data")]
        public object ResponseData { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string ResponseMsg { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string ResponseName { get; set; }
    }
}
