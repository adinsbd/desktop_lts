﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using CS_RFID3_Host_Sample2.RESTFulAPI.Constant;
using CS_RFID3_Host_Sample2.RESTFulAPI.Model;
using Newtonsoft.Json;

namespace CS_RFID3_Host_Sample2.RESTFulAPI.RequestHandler
{
    public class WebClientRequestHandler : InterfaceRequestHandlers
    {
        public string RequestApi(string url, string token, string body)
        {
            WebClientEx client = SetWebClient();
            client.Headers.Add(RequestConstant.Authorization, RequestConstant.AuthorizationValue + token);

            var response = client.UploadString(url, data: body);
            return response;
        }

        public string GetUser(string url, string username, string password)
        {
            LoginParams login = new LoginParams()
            {
                username = username,
                password = password
            };

            string body = JsonConvert.SerializeObject(login);

            WebClientEx client = SetWebClient();

            var response = client.UploadString(url, "POST", body);

            return response;
        }

        private static WebClientEx SetWebClient()
        {
            var client = new WebClientEx();
            client.Headers.Add(RequestConstant.Accept, RequestConstant.AcceptValue);
            client.Headers.Add(RequestConstant.ContentType, RequestConstant.ContentTypeValue);
            client.Timeout = 5000;
            return client;
        }
    }
}
