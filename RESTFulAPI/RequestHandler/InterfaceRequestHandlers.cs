﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS_RFID3_Host_Sample2.RESTFulAPI.RequestHandler
{
    public interface InterfaceRequestHandlers
    {
        string GetUser(string url, string username, string password);

        string RequestApi(string url, string token, string body);
    }
}
