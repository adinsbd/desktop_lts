﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace CS_RFID3_Host_Sample2.RESTFulAPI.RequestHandler
{
    public class WebClientEx : WebClient
    {
        public int Timeout { get; set; }

        protected override WebRequest GetWebRequest(Uri address)
        {
            var request =  base.GetWebRequest(address);
            request.Timeout = Timeout;
            return request;
        }
    }
}
