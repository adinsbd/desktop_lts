﻿
namespace CS_RFID3_Host_Sample2
{
    partial class AppV2Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AppV2Form));
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.functionCallStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.connectionStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.connectionStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.sideMenuPanel = new System.Windows.Forms.Panel();
            this.btnLogout = new System.Windows.Forms.Button();
            this.btnAbout = new System.Windows.Forms.Button();
            this.subMenuMgmtPanel = new System.Windows.Forms.Panel();
            this.btnReboot = new System.Windows.Forms.Button();
            this.btnSoftwareUpdate = new System.Windows.Forms.Button();
            this.btnReadPoint = new System.Windows.Forms.Button();
            this.btnAntennaMode = new System.Windows.Forms.Button();
            this.btnSysInfo = new System.Windows.Forms.Button();
            this.btnManagement = new System.Windows.Forms.Button();
            this.subMenuOperationsPanel = new System.Windows.Forms.Panel();
            this.btnAntennaInfo = new System.Windows.Forms.Button();
            this.btnTriggers = new System.Windows.Forms.Button();
            this.subMenuAccessPanel = new System.Windows.Forms.Panel();
            this.btnBlockErase = new System.Windows.Forms.Button();
            this.btnBlockWrite = new System.Windows.Forms.Button();
            this.btnKill = new System.Windows.Forms.Button();
            this.btnLock = new System.Windows.Forms.Button();
            this.btnWrite = new System.Windows.Forms.Button();
            this.btnRead = new System.Windows.Forms.Button();
            this.btnAccess = new System.Windows.Forms.Button();
            this.subMenuFiltersPanel = new System.Windows.Forms.Panel();
            this.btnAccessFilter = new System.Windows.Forms.Button();
            this.btnPostFilter = new System.Windows.Forms.Button();
            this.btnPreFilter = new System.Windows.Forms.Button();
            this.btnFilters = new System.Windows.Forms.Button();
            this.btnOperations = new System.Windows.Forms.Button();
            this.subMenuConfigPanel = new System.Windows.Forms.Panel();
            this.btnResetFactoryDefault = new System.Windows.Forms.Button();
            this.btnRadioPower = new System.Windows.Forms.Button();
            this.btnSingulation = new System.Windows.Forms.Button();
            this.btnGPIO = new System.Windows.Forms.Button();
            this.btnRFModes = new System.Windows.Forms.Button();
            this.btnAntenna = new System.Windows.Forms.Button();
            this.btnTagStorageSet = new System.Windows.Forms.Button();
            this.btnConfig = new System.Windows.Forms.Button();
            this.readerSubMenuPanel = new System.Windows.Forms.Panel();
            this.btnCapabilities = new System.Windows.Forms.Button();
            this.btnConnection = new System.Windows.Forms.Button();
            this.btnReader = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.autonomous_CB = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.statusStrip.SuspendLayout();
            this.sideMenuPanel.SuspendLayout();
            this.subMenuMgmtPanel.SuspendLayout();
            this.subMenuOperationsPanel.SuspendLayout();
            this.subMenuAccessPanel.SuspendLayout();
            this.subMenuFiltersPanel.SuspendLayout();
            this.subMenuConfigPanel.SuspendLayout();
            this.readerSubMenuPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.autonomous_CB.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.functionCallStatusLabel,
            this.connectionStatusLabel,
            this.connectionStatus});
            this.statusStrip.Location = new System.Drawing.Point(0, 676);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusStrip.Size = new System.Drawing.Size(1067, 25);
            this.statusStrip.TabIndex = 20;
            this.statusStrip.Text = "statusStrip";
            // 
            // functionCallStatusLabel
            // 
            this.functionCallStatusLabel.AutoSize = false;
            this.functionCallStatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.functionCallStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.functionCallStatusLabel.Margin = new System.Windows.Forms.Padding(2, 3, 0, 2);
            this.functionCallStatusLabel.Name = "functionCallStatusLabel";
            this.functionCallStatusLabel.Size = new System.Drawing.Size(762, 20);
            this.functionCallStatusLabel.Text = "Ready";
            this.functionCallStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // connectionStatusLabel
            // 
            this.connectionStatusLabel.Name = "connectionStatusLabel";
            this.connectionStatusLabel.Size = new System.Drawing.Size(0, 20);
            // 
            // connectionStatus
            // 
            this.connectionStatus.AutoSize = false;
            this.connectionStatus.BackgroundImage = global::CS_RFID3_Host_Sample2.Properties.Resources.disconnected;
            this.connectionStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.connectionStatus.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.connectionStatus.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.connectionStatus.Name = "connectionStatus";
            this.connectionStatus.Size = new System.Drawing.Size(90, 20);
            this.connectionStatus.Text = "Disconnected";
            // 
            // sideMenuPanel
            // 
            this.sideMenuPanel.AutoScroll = true;
            this.sideMenuPanel.BackColor = System.Drawing.Color.Black;
            this.sideMenuPanel.Controls.Add(this.btnLogout);
            this.sideMenuPanel.Controls.Add(this.btnAbout);
            this.sideMenuPanel.Controls.Add(this.subMenuMgmtPanel);
            this.sideMenuPanel.Controls.Add(this.btnManagement);
            this.sideMenuPanel.Controls.Add(this.subMenuOperationsPanel);
            this.sideMenuPanel.Controls.Add(this.btnOperations);
            this.sideMenuPanel.Controls.Add(this.subMenuConfigPanel);
            this.sideMenuPanel.Controls.Add(this.btnConfig);
            this.sideMenuPanel.Controls.Add(this.readerSubMenuPanel);
            this.sideMenuPanel.Controls.Add(this.btnReader);
            this.sideMenuPanel.Controls.Add(this.pictureBox1);
            this.sideMenuPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.sideMenuPanel.Location = new System.Drawing.Point(0, 0);
            this.sideMenuPanel.Name = "sideMenuPanel";
            this.sideMenuPanel.Size = new System.Drawing.Size(239, 676);
            this.sideMenuPanel.TabIndex = 21;
            this.sideMenuPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.sideMenuPanel_Paint);
            // 
            // btnLogout
            // 
            this.btnLogout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            this.btnLogout.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnLogout.FlatAppearance.BorderSize = 0;
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.ForeColor = System.Drawing.Color.White;
            this.btnLogout.Location = new System.Drawing.Point(0, 874);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnLogout.Size = new System.Drawing.Size(222, 30);
            this.btnLogout.TabIndex = 10;
            this.btnLogout.Text = "Logout";
            this.btnLogout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLogout.UseVisualStyleBackColor = false;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // btnAbout
            // 
            this.btnAbout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            this.btnAbout.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAbout.FlatAppearance.BorderSize = 0;
            this.btnAbout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAbout.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAbout.ForeColor = System.Drawing.Color.White;
            this.btnAbout.Location = new System.Drawing.Point(0, 844);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnAbout.Size = new System.Drawing.Size(222, 30);
            this.btnAbout.TabIndex = 9;
            this.btnAbout.Text = "About";
            this.btnAbout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAbout.UseVisualStyleBackColor = false;
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // subMenuMgmtPanel
            // 
            this.subMenuMgmtPanel.Controls.Add(this.btnReboot);
            this.subMenuMgmtPanel.Controls.Add(this.btnSoftwareUpdate);
            this.subMenuMgmtPanel.Controls.Add(this.btnReadPoint);
            this.subMenuMgmtPanel.Controls.Add(this.btnAntennaMode);
            this.subMenuMgmtPanel.Controls.Add(this.btnSysInfo);
            this.subMenuMgmtPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.subMenuMgmtPanel.Location = new System.Drawing.Point(0, 727);
            this.subMenuMgmtPanel.Name = "subMenuMgmtPanel";
            this.subMenuMgmtPanel.Size = new System.Drawing.Size(222, 117);
            this.subMenuMgmtPanel.TabIndex = 8;
            // 
            // btnReboot
            // 
            this.btnReboot.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.btnReboot.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnReboot.FlatAppearance.BorderSize = 0;
            this.btnReboot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReboot.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReboot.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnReboot.Location = new System.Drawing.Point(0, 92);
            this.btnReboot.Name = "btnReboot";
            this.btnReboot.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnReboot.Size = new System.Drawing.Size(222, 23);
            this.btnReboot.TabIndex = 48;
            this.btnReboot.Text = "Reboot";
            this.btnReboot.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReboot.UseVisualStyleBackColor = false;
            // 
            // btnSoftwareUpdate
            // 
            this.btnSoftwareUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.btnSoftwareUpdate.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSoftwareUpdate.FlatAppearance.BorderSize = 0;
            this.btnSoftwareUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSoftwareUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSoftwareUpdate.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnSoftwareUpdate.Location = new System.Drawing.Point(0, 69);
            this.btnSoftwareUpdate.Name = "btnSoftwareUpdate";
            this.btnSoftwareUpdate.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnSoftwareUpdate.Size = new System.Drawing.Size(222, 23);
            this.btnSoftwareUpdate.TabIndex = 47;
            this.btnSoftwareUpdate.Text = "Software / Firmware Update";
            this.btnSoftwareUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSoftwareUpdate.UseVisualStyleBackColor = false;
            // 
            // btnReadPoint
            // 
            this.btnReadPoint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.btnReadPoint.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnReadPoint.FlatAppearance.BorderSize = 0;
            this.btnReadPoint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReadPoint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReadPoint.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnReadPoint.Location = new System.Drawing.Point(0, 46);
            this.btnReadPoint.Name = "btnReadPoint";
            this.btnReadPoint.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnReadPoint.Size = new System.Drawing.Size(222, 23);
            this.btnReadPoint.TabIndex = 46;
            this.btnReadPoint.Text = "Read Point";
            this.btnReadPoint.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReadPoint.UseVisualStyleBackColor = false;
            // 
            // btnAntennaMode
            // 
            this.btnAntennaMode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.btnAntennaMode.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAntennaMode.FlatAppearance.BorderSize = 0;
            this.btnAntennaMode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAntennaMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAntennaMode.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnAntennaMode.Location = new System.Drawing.Point(0, 23);
            this.btnAntennaMode.Name = "btnAntennaMode";
            this.btnAntennaMode.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnAntennaMode.Size = new System.Drawing.Size(222, 23);
            this.btnAntennaMode.TabIndex = 45;
            this.btnAntennaMode.Text = "Antenna Mode";
            this.btnAntennaMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAntennaMode.UseVisualStyleBackColor = false;
            // 
            // btnSysInfo
            // 
            this.btnSysInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.btnSysInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSysInfo.FlatAppearance.BorderSize = 0;
            this.btnSysInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSysInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSysInfo.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnSysInfo.Location = new System.Drawing.Point(0, 0);
            this.btnSysInfo.Name = "btnSysInfo";
            this.btnSysInfo.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnSysInfo.Size = new System.Drawing.Size(222, 23);
            this.btnSysInfo.TabIndex = 44;
            this.btnSysInfo.Text = "System Info";
            this.btnSysInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSysInfo.UseVisualStyleBackColor = false;
            // 
            // btnManagement
            // 
            this.btnManagement.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            this.btnManagement.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnManagement.FlatAppearance.BorderSize = 0;
            this.btnManagement.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnManagement.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManagement.ForeColor = System.Drawing.Color.White;
            this.btnManagement.Location = new System.Drawing.Point(0, 697);
            this.btnManagement.Name = "btnManagement";
            this.btnManagement.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnManagement.Size = new System.Drawing.Size(222, 30);
            this.btnManagement.TabIndex = 7;
            this.btnManagement.Text = "Management";
            this.btnManagement.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnManagement.UseVisualStyleBackColor = false;
            this.btnManagement.Click += new System.EventHandler(this.btnManagement_Click);
            // 
            // subMenuOperationsPanel
            // 
            this.subMenuOperationsPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.subMenuOperationsPanel.Controls.Add(this.btnAntennaInfo);
            this.subMenuOperationsPanel.Controls.Add(this.btnTriggers);
            this.subMenuOperationsPanel.Controls.Add(this.subMenuAccessPanel);
            this.subMenuOperationsPanel.Controls.Add(this.btnAccess);
            this.subMenuOperationsPanel.Controls.Add(this.subMenuFiltersPanel);
            this.subMenuOperationsPanel.Controls.Add(this.btnFilters);
            this.subMenuOperationsPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.subMenuOperationsPanel.Location = new System.Drawing.Point(0, 392);
            this.subMenuOperationsPanel.Name = "subMenuOperationsPanel";
            this.subMenuOperationsPanel.Size = new System.Drawing.Size(222, 305);
            this.subMenuOperationsPanel.TabIndex = 7;
            // 
            // btnAntennaInfo
            // 
            this.btnAntennaInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.btnAntennaInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAntennaInfo.FlatAppearance.BorderSize = 0;
            this.btnAntennaInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAntennaInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAntennaInfo.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnAntennaInfo.Location = new System.Drawing.Point(0, 280);
            this.btnAntennaInfo.Name = "btnAntennaInfo";
            this.btnAntennaInfo.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnAntennaInfo.Size = new System.Drawing.Size(222, 23);
            this.btnAntennaInfo.TabIndex = 43;
            this.btnAntennaInfo.Text = "Antenna Info";
            this.btnAntennaInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAntennaInfo.UseVisualStyleBackColor = false;
            // 
            // btnTriggers
            // 
            this.btnTriggers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.btnTriggers.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnTriggers.FlatAppearance.BorderSize = 0;
            this.btnTriggers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTriggers.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTriggers.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnTriggers.Location = new System.Drawing.Point(0, 257);
            this.btnTriggers.Name = "btnTriggers";
            this.btnTriggers.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnTriggers.Size = new System.Drawing.Size(222, 23);
            this.btnTriggers.TabIndex = 42;
            this.btnTriggers.Text = "Triggers";
            this.btnTriggers.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTriggers.UseVisualStyleBackColor = false;
            // 
            // subMenuAccessPanel
            // 
            this.subMenuAccessPanel.Controls.Add(this.btnBlockErase);
            this.subMenuAccessPanel.Controls.Add(this.btnBlockWrite);
            this.subMenuAccessPanel.Controls.Add(this.btnKill);
            this.subMenuAccessPanel.Controls.Add(this.btnLock);
            this.subMenuAccessPanel.Controls.Add(this.btnWrite);
            this.subMenuAccessPanel.Controls.Add(this.btnRead);
            this.subMenuAccessPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.subMenuAccessPanel.Location = new System.Drawing.Point(0, 117);
            this.subMenuAccessPanel.Name = "subMenuAccessPanel";
            this.subMenuAccessPanel.Size = new System.Drawing.Size(222, 140);
            this.subMenuAccessPanel.TabIndex = 41;
            // 
            // btnBlockErase
            // 
            this.btnBlockErase.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnBlockErase.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnBlockErase.FlatAppearance.BorderSize = 0;
            this.btnBlockErase.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBlockErase.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBlockErase.Location = new System.Drawing.Point(0, 115);
            this.btnBlockErase.Name = "btnBlockErase";
            this.btnBlockErase.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnBlockErase.Size = new System.Drawing.Size(222, 23);
            this.btnBlockErase.TabIndex = 43;
            this.btnBlockErase.Text = "Block Erase";
            this.btnBlockErase.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBlockErase.UseVisualStyleBackColor = false;
            // 
            // btnBlockWrite
            // 
            this.btnBlockWrite.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnBlockWrite.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnBlockWrite.FlatAppearance.BorderSize = 0;
            this.btnBlockWrite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBlockWrite.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBlockWrite.Location = new System.Drawing.Point(0, 92);
            this.btnBlockWrite.Name = "btnBlockWrite";
            this.btnBlockWrite.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnBlockWrite.Size = new System.Drawing.Size(222, 23);
            this.btnBlockWrite.TabIndex = 42;
            this.btnBlockWrite.Text = "Block Write";
            this.btnBlockWrite.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBlockWrite.UseVisualStyleBackColor = false;
            // 
            // btnKill
            // 
            this.btnKill.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnKill.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnKill.FlatAppearance.BorderSize = 0;
            this.btnKill.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKill.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKill.Location = new System.Drawing.Point(0, 69);
            this.btnKill.Name = "btnKill";
            this.btnKill.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnKill.Size = new System.Drawing.Size(222, 23);
            this.btnKill.TabIndex = 41;
            this.btnKill.Text = "Kill";
            this.btnKill.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnKill.UseVisualStyleBackColor = false;
            // 
            // btnLock
            // 
            this.btnLock.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnLock.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLock.FlatAppearance.BorderSize = 0;
            this.btnLock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLock.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLock.Location = new System.Drawing.Point(0, 46);
            this.btnLock.Name = "btnLock";
            this.btnLock.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnLock.Size = new System.Drawing.Size(222, 23);
            this.btnLock.TabIndex = 40;
            this.btnLock.Text = "Lock";
            this.btnLock.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLock.UseVisualStyleBackColor = false;
            // 
            // btnWrite
            // 
            this.btnWrite.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnWrite.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnWrite.FlatAppearance.BorderSize = 0;
            this.btnWrite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnWrite.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWrite.Location = new System.Drawing.Point(0, 23);
            this.btnWrite.Name = "btnWrite";
            this.btnWrite.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnWrite.Size = new System.Drawing.Size(222, 23);
            this.btnWrite.TabIndex = 39;
            this.btnWrite.Text = "Write";
            this.btnWrite.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnWrite.UseVisualStyleBackColor = false;
            // 
            // btnRead
            // 
            this.btnRead.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnRead.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRead.FlatAppearance.BorderSize = 0;
            this.btnRead.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRead.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRead.Location = new System.Drawing.Point(0, 0);
            this.btnRead.Name = "btnRead";
            this.btnRead.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnRead.Size = new System.Drawing.Size(222, 23);
            this.btnRead.TabIndex = 38;
            this.btnRead.Text = "Read";
            this.btnRead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRead.UseVisualStyleBackColor = false;
            // 
            // btnAccess
            // 
            this.btnAccess.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.btnAccess.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAccess.FlatAppearance.BorderSize = 0;
            this.btnAccess.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAccess.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAccess.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnAccess.Location = new System.Drawing.Point(0, 94);
            this.btnAccess.Name = "btnAccess";
            this.btnAccess.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnAccess.Size = new System.Drawing.Size(222, 23);
            this.btnAccess.TabIndex = 40;
            this.btnAccess.Text = "Access";
            this.btnAccess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAccess.UseVisualStyleBackColor = false;
            // 
            // subMenuFiltersPanel
            // 
            this.subMenuFiltersPanel.Controls.Add(this.btnAccessFilter);
            this.subMenuFiltersPanel.Controls.Add(this.btnPostFilter);
            this.subMenuFiltersPanel.Controls.Add(this.btnPreFilter);
            this.subMenuFiltersPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.subMenuFiltersPanel.Location = new System.Drawing.Point(0, 23);
            this.subMenuFiltersPanel.Name = "subMenuFiltersPanel";
            this.subMenuFiltersPanel.Size = new System.Drawing.Size(222, 71);
            this.subMenuFiltersPanel.TabIndex = 36;
            this.subMenuFiltersPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.subMenuFiltersPanel_Paint);
            // 
            // btnAccessFilter
            // 
            this.btnAccessFilter.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnAccessFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAccessFilter.FlatAppearance.BorderSize = 0;
            this.btnAccessFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAccessFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAccessFilter.Location = new System.Drawing.Point(0, 46);
            this.btnAccessFilter.Name = "btnAccessFilter";
            this.btnAccessFilter.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnAccessFilter.Size = new System.Drawing.Size(222, 23);
            this.btnAccessFilter.TabIndex = 39;
            this.btnAccessFilter.Text = "Access - Filter";
            this.btnAccessFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAccessFilter.UseVisualStyleBackColor = false;
            // 
            // btnPostFilter
            // 
            this.btnPostFilter.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnPostFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPostFilter.FlatAppearance.BorderSize = 0;
            this.btnPostFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPostFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPostFilter.Location = new System.Drawing.Point(0, 23);
            this.btnPostFilter.Name = "btnPostFilter";
            this.btnPostFilter.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnPostFilter.Size = new System.Drawing.Size(222, 23);
            this.btnPostFilter.TabIndex = 37;
            this.btnPostFilter.Text = "Post - Filter";
            this.btnPostFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPostFilter.UseVisualStyleBackColor = false;
            // 
            // btnPreFilter
            // 
            this.btnPreFilter.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnPreFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPreFilter.FlatAppearance.BorderSize = 0;
            this.btnPreFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPreFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPreFilter.Location = new System.Drawing.Point(0, 0);
            this.btnPreFilter.Name = "btnPreFilter";
            this.btnPreFilter.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnPreFilter.Size = new System.Drawing.Size(222, 23);
            this.btnPreFilter.TabIndex = 36;
            this.btnPreFilter.Text = "Pre - Filter";
            this.btnPreFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPreFilter.UseVisualStyleBackColor = false;
            this.btnPreFilter.Click += new System.EventHandler(this.btnPostFilter_Click);
            // 
            // btnFilters
            // 
            this.btnFilters.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.btnFilters.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnFilters.FlatAppearance.BorderSize = 0;
            this.btnFilters.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFilters.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFilters.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnFilters.Location = new System.Drawing.Point(0, 0);
            this.btnFilters.Name = "btnFilters";
            this.btnFilters.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnFilters.Size = new System.Drawing.Size(222, 23);
            this.btnFilters.TabIndex = 35;
            this.btnFilters.Text = "Filters";
            this.btnFilters.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFilters.UseVisualStyleBackColor = false;
            // 
            // btnOperations
            // 
            this.btnOperations.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            this.btnOperations.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnOperations.FlatAppearance.BorderSize = 0;
            this.btnOperations.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOperations.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOperations.ForeColor = System.Drawing.Color.White;
            this.btnOperations.Location = new System.Drawing.Point(0, 362);
            this.btnOperations.Name = "btnOperations";
            this.btnOperations.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnOperations.Size = new System.Drawing.Size(222, 30);
            this.btnOperations.TabIndex = 6;
            this.btnOperations.Text = "Operations";
            this.btnOperations.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOperations.UseVisualStyleBackColor = false;
            this.btnOperations.Click += new System.EventHandler(this.btnOperations_Click);
            // 
            // subMenuConfigPanel
            // 
            this.subMenuConfigPanel.Controls.Add(this.btnResetFactoryDefault);
            this.subMenuConfigPanel.Controls.Add(this.btnRadioPower);
            this.subMenuConfigPanel.Controls.Add(this.btnSingulation);
            this.subMenuConfigPanel.Controls.Add(this.btnGPIO);
            this.subMenuConfigPanel.Controls.Add(this.btnRFModes);
            this.subMenuConfigPanel.Controls.Add(this.btnAntenna);
            this.subMenuConfigPanel.Controls.Add(this.btnTagStorageSet);
            this.subMenuConfigPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.subMenuConfigPanel.Location = new System.Drawing.Point(0, 189);
            this.subMenuConfigPanel.Name = "subMenuConfigPanel";
            this.subMenuConfigPanel.Size = new System.Drawing.Size(222, 173);
            this.subMenuConfigPanel.TabIndex = 5;
            // 
            // btnResetFactoryDefault
            // 
            this.btnResetFactoryDefault.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.btnResetFactoryDefault.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnResetFactoryDefault.FlatAppearance.BorderSize = 0;
            this.btnResetFactoryDefault.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnResetFactoryDefault.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResetFactoryDefault.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnResetFactoryDefault.Location = new System.Drawing.Point(0, 150);
            this.btnResetFactoryDefault.Name = "btnResetFactoryDefault";
            this.btnResetFactoryDefault.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnResetFactoryDefault.Size = new System.Drawing.Size(222, 25);
            this.btnResetFactoryDefault.TabIndex = 7;
            this.btnResetFactoryDefault.Text = "Reset to Factory Default";
            this.btnResetFactoryDefault.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnResetFactoryDefault.UseVisualStyleBackColor = false;
            // 
            // btnRadioPower
            // 
            this.btnRadioPower.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.btnRadioPower.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRadioPower.FlatAppearance.BorderSize = 0;
            this.btnRadioPower.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRadioPower.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRadioPower.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnRadioPower.Location = new System.Drawing.Point(0, 125);
            this.btnRadioPower.Name = "btnRadioPower";
            this.btnRadioPower.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnRadioPower.Size = new System.Drawing.Size(222, 25);
            this.btnRadioPower.TabIndex = 6;
            this.btnRadioPower.Text = "Radio Power";
            this.btnRadioPower.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRadioPower.UseVisualStyleBackColor = false;
            // 
            // btnSingulation
            // 
            this.btnSingulation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.btnSingulation.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSingulation.FlatAppearance.BorderSize = 0;
            this.btnSingulation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSingulation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSingulation.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnSingulation.Location = new System.Drawing.Point(0, 100);
            this.btnSingulation.Name = "btnSingulation";
            this.btnSingulation.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnSingulation.Size = new System.Drawing.Size(222, 25);
            this.btnSingulation.TabIndex = 5;
            this.btnSingulation.Text = "Singulation";
            this.btnSingulation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSingulation.UseVisualStyleBackColor = false;
            // 
            // btnGPIO
            // 
            this.btnGPIO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.btnGPIO.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnGPIO.FlatAppearance.BorderSize = 0;
            this.btnGPIO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGPIO.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGPIO.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnGPIO.Location = new System.Drawing.Point(0, 75);
            this.btnGPIO.Name = "btnGPIO";
            this.btnGPIO.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnGPIO.Size = new System.Drawing.Size(222, 25);
            this.btnGPIO.TabIndex = 4;
            this.btnGPIO.Text = "GPIO";
            this.btnGPIO.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGPIO.UseVisualStyleBackColor = false;
            // 
            // btnRFModes
            // 
            this.btnRFModes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.btnRFModes.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRFModes.FlatAppearance.BorderSize = 0;
            this.btnRFModes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRFModes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRFModes.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnRFModes.Location = new System.Drawing.Point(0, 50);
            this.btnRFModes.Name = "btnRFModes";
            this.btnRFModes.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnRFModes.Size = new System.Drawing.Size(222, 25);
            this.btnRFModes.TabIndex = 3;
            this.btnRFModes.Text = "RF Modes";
            this.btnRFModes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRFModes.UseVisualStyleBackColor = false;
            this.btnRFModes.Click += new System.EventHandler(this.btnRFModes_Click);
            // 
            // btnAntenna
            // 
            this.btnAntenna.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.btnAntenna.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAntenna.FlatAppearance.BorderSize = 0;
            this.btnAntenna.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAntenna.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAntenna.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnAntenna.Location = new System.Drawing.Point(0, 25);
            this.btnAntenna.Name = "btnAntenna";
            this.btnAntenna.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnAntenna.Size = new System.Drawing.Size(222, 25);
            this.btnAntenna.TabIndex = 2;
            this.btnAntenna.Text = "Antenna";
            this.btnAntenna.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAntenna.UseVisualStyleBackColor = false;
            // 
            // btnTagStorageSet
            // 
            this.btnTagStorageSet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.btnTagStorageSet.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnTagStorageSet.FlatAppearance.BorderSize = 0;
            this.btnTagStorageSet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTagStorageSet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTagStorageSet.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnTagStorageSet.Location = new System.Drawing.Point(0, 0);
            this.btnTagStorageSet.Name = "btnTagStorageSet";
            this.btnTagStorageSet.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnTagStorageSet.Size = new System.Drawing.Size(222, 25);
            this.btnTagStorageSet.TabIndex = 1;
            this.btnTagStorageSet.Text = "Tag Storage Setting";
            this.btnTagStorageSet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTagStorageSet.UseVisualStyleBackColor = false;
            // 
            // btnConfig
            // 
            this.btnConfig.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            this.btnConfig.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnConfig.FlatAppearance.BorderSize = 0;
            this.btnConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfig.ForeColor = System.Drawing.Color.White;
            this.btnConfig.Location = new System.Drawing.Point(0, 159);
            this.btnConfig.Name = "btnConfig";
            this.btnConfig.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnConfig.Size = new System.Drawing.Size(222, 30);
            this.btnConfig.TabIndex = 4;
            this.btnConfig.Text = "Config";
            this.btnConfig.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfig.UseVisualStyleBackColor = false;
            this.btnConfig.Click += new System.EventHandler(this.btnConfig_Click);
            // 
            // readerSubMenuPanel
            // 
            this.readerSubMenuPanel.Controls.Add(this.btnCapabilities);
            this.readerSubMenuPanel.Controls.Add(this.btnConnection);
            this.readerSubMenuPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.readerSubMenuPanel.Location = new System.Drawing.Point(0, 112);
            this.readerSubMenuPanel.Name = "readerSubMenuPanel";
            this.readerSubMenuPanel.Size = new System.Drawing.Size(222, 47);
            this.readerSubMenuPanel.TabIndex = 2;
            // 
            // btnCapabilities
            // 
            this.btnCapabilities.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.btnCapabilities.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapabilities.FlatAppearance.BorderSize = 0;
            this.btnCapabilities.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCapabilities.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapabilities.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCapabilities.Location = new System.Drawing.Point(0, 25);
            this.btnCapabilities.Name = "btnCapabilities";
            this.btnCapabilities.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnCapabilities.Size = new System.Drawing.Size(222, 25);
            this.btnCapabilities.TabIndex = 1;
            this.btnCapabilities.Text = "Capabilities";
            this.btnCapabilities.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCapabilities.UseVisualStyleBackColor = false;
            // 
            // btnConnection
            // 
            this.btnConnection.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.btnConnection.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnConnection.FlatAppearance.BorderSize = 0;
            this.btnConnection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnection.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnConnection.Location = new System.Drawing.Point(0, 0);
            this.btnConnection.Name = "btnConnection";
            this.btnConnection.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnConnection.Size = new System.Drawing.Size(222, 25);
            this.btnConnection.TabIndex = 0;
            this.btnConnection.Text = "Connection";
            this.btnConnection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConnection.UseVisualStyleBackColor = false;
            this.btnConnection.Click += new System.EventHandler(this.btnConnection_Click);
            // 
            // btnReader
            // 
            this.btnReader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            this.btnReader.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnReader.FlatAppearance.BorderSize = 0;
            this.btnReader.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReader.ForeColor = System.Drawing.Color.White;
            this.btnReader.Location = new System.Drawing.Point(0, 82);
            this.btnReader.Name = "btnReader";
            this.btnReader.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnReader.Size = new System.Drawing.Size(222, 30);
            this.btnReader.TabIndex = 1;
            this.btnReader.Text = "Reader";
            this.btnReader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReader.UseVisualStyleBackColor = false;
            this.btnReader.Click += new System.EventHandler(this.btnReader_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(222, 82);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // mainPanel
            // 
            this.mainPanel.AutoSize = true;
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.mainPanel.Location = new System.Drawing.Point(239, 0);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(828, 0);
            this.mainPanel.TabIndex = 22;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.autonomous_CB);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(239, 603);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(828, 73);
            this.panel1.TabIndex = 23;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // autonomous_CB
            // 
            this.autonomous_CB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.autonomous_CB.Controls.Add(this.label13);
            this.autonomous_CB.Location = new System.Drawing.Point(3, 3);
            this.autonomous_CB.Name = "autonomous_CB";
            this.autonomous_CB.Size = new System.Drawing.Size(822, 66);
            this.autonomous_CB.TabIndex = 21;
            this.autonomous_CB.TabStop = false;
            this.autonomous_CB.Text = "GPI States";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 27);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "Red For Low";
            // 
            // AppV2Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 701);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.sideMenuPanel);
            this.Controls.Add(this.statusStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AppV2Form";
            this.Text = "LTS";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.AppV2Form_Load);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.sideMenuPanel.ResumeLayout(false);
            this.subMenuMgmtPanel.ResumeLayout(false);
            this.subMenuOperationsPanel.ResumeLayout(false);
            this.subMenuAccessPanel.ResumeLayout(false);
            this.subMenuFiltersPanel.ResumeLayout(false);
            this.subMenuConfigPanel.ResumeLayout(false);
            this.readerSubMenuPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.autonomous_CB.ResumeLayout(false);
            this.autonomous_CB.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

            this.connectBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.functionCallStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.connectionStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.connectionStatus = new System.Windows.Forms.ToolStripStatusLabel();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        internal System.Windows.Forms.ToolStripStatusLabel functionCallStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel connectionStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel connectionStatus;
        private System.Windows.Forms.Panel sideMenuPanel;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel readerSubMenuPanel;
        private System.Windows.Forms.Button btnReader;
        private System.Windows.Forms.Button btnCapabilities;
        private System.Windows.Forms.Button btnConnection;
        private System.Windows.Forms.Button btnConfig;
        private System.Windows.Forms.Panel subMenuConfigPanel;
        private System.Windows.Forms.Button btnGPIO;
        private System.Windows.Forms.Button btnRFModes;
        private System.Windows.Forms.Button btnAntenna;
        private System.Windows.Forms.Button btnTagStorageSet;
        private System.Windows.Forms.Button btnRadioPower;
        private System.Windows.Forms.Button btnSingulation;
        private System.Windows.Forms.Button btnResetFactoryDefault;
        private System.Windows.Forms.Button btnOperations;
        private System.Windows.Forms.Button btnManagement;
        private System.Windows.Forms.Panel subMenuOperationsPanel;
        private System.Windows.Forms.Panel subMenuAccessPanel;
        private System.Windows.Forms.Button btnRead;
        private System.Windows.Forms.Button btnAccess;
        private System.Windows.Forms.Panel subMenuFiltersPanel;
        private System.Windows.Forms.Button btnAccessFilter;
        private System.Windows.Forms.Button btnPostFilter;
        private System.Windows.Forms.Button btnPreFilter;
        private System.Windows.Forms.Button btnFilters;
        private System.Windows.Forms.Button btnBlockWrite;
        private System.Windows.Forms.Button btnKill;
        private System.Windows.Forms.Button btnLock;
        private System.Windows.Forms.Button btnWrite;
        private System.Windows.Forms.Button btnBlockErase;
        private System.Windows.Forms.Button btnAntennaInfo;
        private System.Windows.Forms.Button btnTriggers;
        private System.Windows.Forms.Panel subMenuMgmtPanel;
        private System.Windows.Forms.Button btnReadPoint;
        private System.Windows.Forms.Button btnAntennaMode;
        private System.Windows.Forms.Button btnSysInfo;
        private System.Windows.Forms.Button btnReboot;
        private System.Windows.Forms.Button btnSoftwareUpdate;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Button btnAbout;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox autonomous_CB;
        private System.Windows.Forms.Label label13;

        internal System.ComponentModel.BackgroundWorker connectBackgroundWorker;
    }
}