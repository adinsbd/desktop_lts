﻿using CS_RFID3_Host_Sample2.Extentions;
using CS_RFID3_Host_Sample2.RESTFulAPI.Constant;
using CS_RFID3_Host_Sample2.RESTFulAPI.Model;
using CS_RFID3_Host_Sample2.RESTFulAPI.RequestHandler;
using CS_RFID3_Host_Sample2.Static;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Symbol.RFID3;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CS_RFID3_Host_Sample2
{
    public partial class LinenRegisterForm : Form
    {

        //private AppForm m_AppForm;

        public static JArray arrCustomerData;

        public Dictionary<long, string> customerDic;
        public Dictionary<long, string> ruanganDic;
        public Dictionary<long, string> linenDic;
        public Dictionary<long, string> statusLinenDic;
        public Dictionary<long, string> reasonDic;

        public static List<Customer> customers = new List<Customer>();

        private RFIDReader readerAPI;
        private PostFilterForm pff;
        private TriggersForm tf;
        private AntennaInfoForm aif;
        private AccessFilterForm aff;
        private TagData[] readTag;
        private Hashtable tagTable;

        private bool isConnected;

        public LinenRegisterForm(RFIDReader readerAPI, bool m_IsConnected, PostFilterForm m_PostFilterForm,
            TriggersForm m_TriggerForm, AntennaInfoForm m_AntennaInfoForm, AccessFilterForm m_AccessFilterForm, System.Collections.Hashtable m_TagTable)
        {
            InitializeComponent();

            this.readerAPI = readerAPI;
            this.isConnected = m_IsConnected;
            this.pff = m_PostFilterForm;
            this.tf = m_TriggerForm;
            this.aif = m_AntennaInfoForm;
            this.aff = m_AccessFilterForm;
            this.tagTable = m_TagTable;

            this.readTag = new TagData[1];

            this.customerDic = new Dictionary<long, string>();
            this.ruanganDic = new Dictionary<long, string>();
            this.linenDic = new Dictionary<long, string>();
            this.statusLinenDic = new Dictionary<long, string>();
            this.reasonDic = new Dictionary<long, string>();

            SetCustomerComboBox();

            LoadNewRegisteredLinen();
        }

        private void LoadNewRegisteredLinen()
        {
            listRegisteredLinen.Items.Clear();
            InterfaceRequestHandlers webRequestHandlers = new WebClientRequestHandler();
            try
            {
                var response = GetNewRegisteredLinen(webRequestHandlers, UserStatic.userToken);
                var linenLists = JsonConvert.DeserializeObject<ResponseJson>(response);
                if(linenLists.Status == "true")
                {
                    JObject jObj = JObject.FromObject(linenLists.ResponseData);
                    JArray arrLinenData = (JArray)jObj.SelectToken("data");
                    foreach(var linen in arrLinenData)
                    {
                        FillListViewOfRegisteredLinen(linen);

                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            //throw new NotImplementedException();
        }

        private void FillListViewOfRegisteredLinen(JToken linen)
        {
            JObject linenObj = JObject.FromObject(linen);
            string rfid = (string)linenObj.SelectToken("item_linen_rfid");
            string linenName = (string)linenObj.SelectToken("item_product_name");
            string custName = (string)linenObj.SelectToken("company_name");
            string ruangan = (string)linenObj.SelectToken("location_name");
            string sessionRegistered = (string)linenObj.SelectToken("item_linen_created_at");
            string registeredDate = sessionRegistered.Split(' ')[0];
            string registeredBY = (string)linenObj.SelectToken("name");
            string linenStatus = (string)linenObj.SelectToken("item_linen_rent");

            ListViewItem listView = new ListViewItem(rfid);
            ListViewItem.ListViewSubItem subItem;

            subItem = new ListViewItem.ListViewSubItem(listView, linenName);
            listView.SubItems.Add(subItem);

            subItem = new ListViewItem.ListViewSubItem(listView, custName);
            listView.SubItems.Add(subItem);

            subItem = new ListViewItem.ListViewSubItem(listView, ruangan);
            listView.SubItems.Add(subItem);

            subItem = new ListViewItem.ListViewSubItem(listView, registeredDate);
            listView.SubItems.Add(subItem);

            subItem = new ListViewItem.ListViewSubItem(listView, registeredBY);
            listView.SubItems.Add(subItem);

            subItem = new ListViewItem.ListViewSubItem(listView, linenStatus);
            listView.SubItems.Add(subItem);

            listRegisteredLinen.BeginUpdate();
            listRegisteredLinen.Items.Add(listView);
            listRegisteredLinen.EndUpdate();
        }

        private static string GetNewRegisteredLinen(InterfaceRequestHandlers webRequestHandlers, string userToken)
        {
            string url = RequestConstant.BaseUrl + RequestConstant.LinenPath + RequestConstant.GetData;
            JObject jObj = new JObject();
            jObj.Add("search", "dreg_");
            jObj.Add("code", "item_linen_session");
            string body = JsonConvert.SerializeObject(jObj);
            Console.WriteLine("Hit url : " + url);
            return webRequestHandlers.RequestApi(url, userToken, body);
            
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnDeleteItem_Click(object sender, EventArgs e)
        {
            for(int i=0; i < listViewPrepareRegister.Items.Count; i++)
            {
                if (listViewPrepareRegister.Items[i].Selected)
                {
                    listViewPrepareRegister.Items[i].Remove();
                    i--;
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void SetCustomerComboBox()
        {
            InterfaceRequestHandlers webRequestHandlers = new WebClientRequestHandler();

            try
            {
                Console.WriteLine("Call Customer API...");
                var response = GetCustomer(webRequestHandlers, UserStatic.userToken);
                var customerLists = JsonConvert.DeserializeObject< ResponseJson > (response);

                if(customerLists.Status == "true")
                {
                    Console.WriteLine("Generate jObj...");
                    JObject jObj = JObject.FromObject(customerLists.ResponseData);
                    
                    Console.WriteLine("Get list data...");
                    arrCustomerData = (JArray)jObj.SelectToken("data");

                    //Console.WriteLine("Customer : " + arrData.ToString());
                    var i = 1;
                    foreach (var item in arrCustomerData)
                    {
                        //Customer customer = JsonConvert.DeserializeObject<Customer>(item);
                        JObject custObj = JObject.FromObject(item);
                        Customer cust = new Customer();
                        cust.CompanyId = (long)custObj.SelectToken("company_id");
                        cust.CompanyName = (string)custObj.SelectToken("company_name");
                        cust.Locations = (JArray)custObj.SelectToken("locations");
                        cust.Products = (JArray)custObj.SelectToken("products");
                        customers.Add(cust);

                        //set combobox here
                        customerDic.Add((long)custObj.SelectToken("company_id"), (string)custObj.SelectToken("company_name"));

                        Console.WriteLine("Customer " + i);
                        Console.WriteLine(item);
                        i++;
                    }

                    JArray arrStatus = (JArray)jObj.SelectToken("rental");
                    foreach(var itemStatus in arrStatus)
                    {
                        JObject statusObj = JObject.FromObject(itemStatus);

                        statusLinenDic.Add((long)statusObj.SelectToken("id"), (string)statusObj.SelectToken("name"));
                    }

                    JArray arrReason = (JArray)jObj.SelectToken("status");
                    foreach (var itemStatus in arrReason)
                    {
                        JObject statusObj = JObject.FromObject(itemStatus);

                        reasonDic.Add((long)statusObj.SelectToken("id"), (string)statusObj.SelectToken("name"));
                    }

                    cbNamaCustomer.DataSource = new BindingSource(customerDic, null);
                    cbNamaCustomer.DisplayMember = "Value";
                    cbNamaCustomer.ValueMember = "Key";
                    cbNamaCustomer.Text = "";
                    
                    cbStatusLinen.DataSource = new BindingSource(statusLinenDic, null);
                    cbStatusLinen.DisplayMember = "Value";
                    cbStatusLinen.ValueMember = "Key";
                    cbStatusLinen.Text = "";

                    cbKebutuhan.DataSource = new BindingSource(reasonDic, null);
                    cbKebutuhan.DisplayMember = "Value";
                    cbKebutuhan.ValueMember = "Key";
                    cbKebutuhan.Text = "";

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            //throw new NotImplementedException();
        }

        public static string GetCustomer(InterfaceRequestHandlers webRequestHandlers, string token)
        {
            string url = RequestConstant.BaseUrl + RequestConstant.CustomerPath + RequestConstant.GetData;
            string body = "";
            Console.WriteLine("Hit url : " + url);
            return webRequestHandlers.RequestApi(url, token, body);
        }

        private void cbNamaCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            Console.WriteLine("clear ruangan");
            cbRuangan.DataSource = null;
            //cbRuangan.Items.Clear();
            cbRuangan.Text = "";

            Console.WriteLine("clear linen");
            cbRuangan.DataSource = null;
            //cbNamaLinen.Items.Clear();
            cbNamaLinen.Text = "";
            SetAnotherComboboxAfterCbCustomer();


            //Console.WriteLine("Customer Selected : " + filtered.ToString());
        }

        private void SetAnotherComboboxAfterCbCustomer()
        {
            this.ruanganDic = new Dictionary<long, string>();
            this.linenDic = new Dictionary<long, string>();
            
            string customer = ((KeyValuePair<long, string>)cbNamaCustomer.SelectedItem).Value;

            List<Customer> filtered = customers.Where(x => x.CompanyName == customer).ToList();

            foreach (var custRoom in filtered[0].Locations)
            {
                JObject roomObj = JObject.FromObject(custRoom);

                ruanganDic.Add((long)roomObj.SelectToken("location_id"), (string)roomObj.SelectToken("location_name"));
            }

            foreach (var custLinen in filtered[0].Products)
            {
                JObject productObj = JObject.FromObject(custLinen);

                linenDic.Add((long)productObj.SelectToken("item_product_id"), (string)productObj.SelectToken("item_product_name"));
            }
            if (filtered[0].Locations.Count > 0)
            {
                //ruanganDic.Add(0, "(empty)");
                cbRuangan.DataSource = new BindingSource(ruanganDic, null);
                cbRuangan.DisplayMember = "Value";
                cbRuangan.ValueMember = "Key";
            }


            if (filtered[0].Products.Count > 0)
            {
                //linenDic.Add(0, "(empty)");
                cbNamaLinen.DataSource = new BindingSource(linenDic, null);
                cbNamaLinen.DisplayMember = "Value";
                cbNamaLinen.ValueMember = "Key";
            }
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (isConnected)
                {
                    if(btnRead.Text == "Read")
                    {
                        if (readerAPI.Actions.TagAccess.OperationSequence.Length > 0)
                        {
                            readerAPI.Actions.TagAccess.OperationSequence.PerformSequence(aff.getFilter(),
                                tf.getTriggerInfo(), aif.getInfo());
                        }
                        else
                        {
                            readerAPI.Actions.Inventory.Perform(
                              pff.getFilter(),
                              tf.getTriggerInfo(),
                              aif.getInfo()
                           );
                        }

                        Console.WriteLine("Get Read Tags...");
                        Symbol.RFID3.TagData[] tagDatas = readerAPI.Actions.GetReadTags(10);
                        if (null != tagDatas)
                        {
                            /*TagData td = tagDatas[0];
                            txtRFID.Text = td.TagID;
                            Console.WriteLine("Tag : " + td.TagID);*/
                            listViewPrepareRegister.BeginUpdate();
                            foreach (TagData td in tagDatas)
                            {
                                long locationId, productId, companyId, rent;

                                locationId = ((KeyValuePair<long, string>)cbRuangan.SelectedItem).Key;
                                productId = ((KeyValuePair<long, string>)cbNamaLinen.SelectedItem).Key;
                                companyId = ((KeyValuePair<long, string>)cbNamaCustomer.SelectedItem).Key;

                                bool isFound = listViewPrepareRegister.FindItemWithText(td.TagID) != null;

                                Console.WriteLine("Is " + td.TagID + " found ? " + isFound);

                                txtRFID.Text = td.TagID;

                                if (!isFound)
                                {
                                    ListViewItem listView = new ListViewItem(txtRFID.Text);
                                    ListViewItem.ListViewSubItem subItem;

                                    subItem = new ListViewItem.ListViewSubItem(listView, cbNamaLinen.Text);
                                    listView.SubItems.Add(subItem);

                                    subItem = new ListViewItem.ListViewSubItem(listView, cbNamaCustomer.Text);
                                    listView.SubItems.Add(subItem);

                                    subItem = new ListViewItem.ListViewSubItem(listView, cbRuangan.Text);
                                    listView.SubItems.Add(subItem);

                                    subItem = new ListViewItem.ListViewSubItem(listView, cbStatusLinen.Text);
                                    listView.SubItems.Add(subItem);

                                    
                                    listViewPrepareRegister.Items.Add(listView);
                                    
                                }                                                             
                            }
                            listViewPrepareRegister.EndUpdate();

                        }
                       
                        btnRead.Text = "Stop";
                    }
                    else if(btnRead.Text == "Stop")
                    {
                        readerAPI.Actions.Inventory.Stop();

                        /*
                         * check Existing Linen by RFID
                         */
                        /*if(txtRFID.Text == "")
                        {
                            MessageBox.Show("Error : RFID tidak terdeteksi.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        else
                        {
                            long locationId, productId, companyId, rent;
                            string rfid, body;
                            bool isUpdate = false;
                            JObject linenIsExist = LinenIsExist(txtRFID.Text);
                            int total = (int)linenIsExist.SelectToken("total");
                            if (total > 0)
                            {
                                Console.WriteLine("RFID " + txtRFID.Text + " exists . . .");
                                JArray arrLinen = (JArray)linenIsExist.SelectToken("data");
                                JObject linenObj = JObject.FromObject(arrLinen[0]);

                                cbNamaCustomer.Text = (string)linenObj.SelectToken("company_name");

                                Console.WriteLine("clear ruangan");
                                cbRuangan.DataSource = null;
                                //cbRuangan.Items.Clear();
                                cbRuangan.Text = "";

                                Console.WriteLine("clear linen");
                                cbRuangan.DataSource = null;
                                //cbNamaLinen.Items.Clear();
                                cbNamaLinen.Text = "";

                                SetAnotherComboboxAfterCbCustomer();

                                cbRuangan.Text = (string)linenObj.SelectToken("location_name");
                                cbNamaLinen.Text = (string)linenObj.SelectToken("item_product_name");
                                cbStatusLinen.Text = (string)linenObj.SelectToken("item_linen_rent");
                            }
                            else
                            {
                                if (cbNamaCustomer.Text == "" || cbRuangan.Text == "" || cbNamaLinen.Text == "" || cbStatusLinen.Text == "")
                                {
                                    MessageBox.Show("Error : Untuk RFID baru, semua field harus dipilih.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }
                                else
                                {
                                    Console.WriteLine("Set Linen");
                                    SetLinenParams(out locationId, out productId, out companyId, out rfid, out rent, out body, isUpdate);
                                    var response = RegisterLinen(body, isUpdate);
                                    GetResultOfSaveOrUpdate(response);

                                    LoadNewRegisteredLinen();
                                }
                               
                            }
                            
                                                    
                        }*/

                        btnRead.Text = "Read";
                        //btnUpdate.Enabled = true;
                    }
                }

            }
            catch (InvalidOperationException ioe)
            {
                MessageBox.Show("Error : " + ioe.Message, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (InvalidUsageException iue)
            {
                MessageBox.Show("Error : " + iue.Message, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (OperationFailureException ofe)
            {
                MessageBox.Show("Error : " + ofe.Message, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void GetResultOfSaveOrUpdate(List<string> failedRFID)
        {
            int errorCount = failedRFID.Count;
            if (errorCount == 0)
            {
                //MessageBox.Show("Semua data linen berhasil diregistrasi.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.alertNotif("Semua data linen berhasil diregistrasi.", AlertForm.enmType.Success);
            }
            else
            {
                //MessageBox.Show(10 - errorCount+ " data linen berhasil diregistrasi.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.alertNotif(10 - errorCount + " data linen berhasil diregistrasi.", AlertForm.enmType.Success);
            }

            cbNamaCustomer.Text = "";
            cbRuangan.Text = "";
            cbNamaLinen.Text = "";
            cbStatusLinen.Text = "";
            txtRFID.Text = "";

            listViewPrepareRegister.Items.Clear();

            txtRFID.Focus();
            //Console.WriteLine(companyId + " " + locationId + " " + productId + " " + rent + " " + rfid);
        }

        private void SetLinenParams(out long locationId, out long productId, out long companyId, string rfid, out long rent, out string body, bool isUpdate)
        {
            locationId = ((KeyValuePair<long, string>)cbRuangan.SelectedItem).Key;
            productId = ((KeyValuePair<long, string>)cbNamaLinen.SelectedItem).Key;
            companyId = ((KeyValuePair<long, string>)cbNamaCustomer.SelectedItem).Key;
            rent = ((KeyValuePair<long, string>)cbStatusLinen.SelectedItem).Key;
            long reason = ((KeyValuePair<long, string>)cbKebutuhan.SelectedItem).Key;

            string regSession = "dreg_" + UserStatic.loginTimeStamp;

            LinenParams lp = new LinenParams
            {
                item_linen_company_id = companyId,
                item_linen_location_id = locationId,
                item_linen_product_id = productId,
                item_linen_rent = rent,
                item_linen_rfid = rfid,
                item_linen_status = reason,
                item_linen_session = regSession
            };

            JObject jo = JObject.FromObject(lp);

            if(isUpdate)
                jo.Add("type", "update");

            body = JsonConvert.SerializeObject(jo);
        }

        private static string RegisterLinen(string body, bool isUpdate)
        {
            Console.WriteLine("Register linen...");
            Console.WriteLine("Body : " + body);
            InterfaceRequestHandlers webRequestHandlers = new WebClientRequestHandler();
            string url;
            if (!isUpdate)
            {
                url = RequestConstant.BaseUrl + RequestConstant.LinenPath + RequestConstant.SaveData;
            }
            else
            {
                url = RequestConstant.BaseUrl + RequestConstant.LinenPath + RequestConstant.PatchData;
            }
            string token = UserStatic.userToken;
            return webRequestHandlers.RequestApi(url, token, body);
        }

        private static JObject LinenIsExist(string rfid)
        {
            //int total = 0;
            Console.WriteLine("check linen ...");
            JObject jObj = new JObject();
            try
            {           
                InterfaceRequestHandlers webRequestHandlers = new WebClientRequestHandler();
                var response = CheckLinen(webRequestHandlers, UserStatic.userToken, rfid);
                var linen = JsonConvert.DeserializeObject<ResponseJson>(response);
                if(linen.Status == "true")
                {
                    Console.WriteLine("API Status is true...");
                    jObj = JObject.FromObject(linen.ResponseData);
                    //total = (int)jObj.SelectToken("total");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
            return jObj;
        }

        private static string CheckLinen(InterfaceRequestHandlers webRequestHandlers, string token, string rfid)
        {
            string url = RequestConstant.BaseUrl + RequestConstant.LinenPath + RequestConstant.GetData;
            PostDataParams pdp = new PostDataParams
            {
                search = rfid,
            };
            string body = JsonConvert.SerializeObject(pdp);

            Console.WriteLine("Hit url : " + url);
            Console.WriteLine("Body : " + body);
            return webRequestHandlers.RequestApi(url, token, body);
        }

        private void cbStatusLinen_SelectedIndexChanged(object sender, EventArgs e)
        {

            
            /*if(cbNamaCustomer.SelectedIndex > -1 && cbRuangan.SelectedIndex > -1 && cbNamaLinen.SelectedIndex > -1 && cbStatusLinen.SelectedIndex > -1)
            {
                txtRFID.Enabled = true;
                btnRead.Enabled = true;
            }
            else
            {
                txtRFID.Enabled = false;
                btnRead.Enabled = false;
            }*/
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            List<string> failedRFID = new List<string>();

            long locationId, productId, companyId, rent;
            string rfid, body;
            bool isUpdate = false;

            try
            {
                foreach (ListViewItem item in listViewPrepareRegister.Items)
                {
                    //Console.WriteLine(item.Text);
                    rfid = item.Text;
                    SetLinenParams(out locationId, out productId, out companyId, rfid, out rent, out body, isUpdate);
                    var response = RegisterLinen(body, isUpdate);
                    var result = JsonConvert.DeserializeObject<ResponseJson>(response);
                    if (result.Status != "true")
                    {
                        failedRFID.Add(rfid);
                    }
                    //GetResultOfSaveOrUpdate(response);
                }

                GetResultOfSaveOrUpdate(failedRFID);

                LoadNewRegisteredLinen();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        public void alertNotif(string msg, AlertForm.enmType type)
        {
            AlertForm af = new AlertForm();
            af.showAlert(msg, type);
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
