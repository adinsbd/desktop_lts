namespace CS_RFID3_Host_Sample2
{
    partial class AppForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AppForm));
            this.gpiLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.functionCallStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.connectionStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.connectionStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.gpiStateGB = new System.Windows.Forms.GroupBox();
            this.gpiNumberLabel = new System.Windows.Forms.Label();
            this.transmitPowerGB = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.hScrollBar1 = new System.Windows.Forms.HScrollBar();
            this.dataContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tagDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readDataContextMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeDataContextMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lockDataContextMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.killDataContextMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blockWriteDataContextMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blockEraseDataContextMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.locateTagToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.connectBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.accessBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.sideMenuPanel = new System.Windows.Forms.Panel();
            this.btnLogout = new System.Windows.Forms.Button();
            this.btnAbout = new System.Windows.Forms.Button();
            this.subMenuOperationsPanel = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnNewRegisteredLinen = new System.Windows.Forms.Button();
            this.btnOperations = new System.Windows.Forms.Button();
            this.subMenuConfigPanel = new System.Windows.Forms.Panel();
            this.btnGrouping = new System.Windows.Forms.Button();
            this.btnLinenRegister = new System.Windows.Forms.Button();
            this.btnMasterData = new System.Windows.Forms.Button();
            this.readerSubMenuPanel = new System.Windows.Forms.Panel();
            this.btnAntenna = new System.Windows.Forms.Button();
            this.btnConnection = new System.Windows.Forms.Button();
            this.btnReader = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.statusStrip.SuspendLayout();
            this.gpiStateGB.SuspendLayout();
            this.transmitPowerGB.SuspendLayout();
            this.dataContextMenuStrip.SuspendLayout();
            this.sideMenuPanel.SuspendLayout();
            this.subMenuOperationsPanel.SuspendLayout();
            this.subMenuConfigPanel.SuspendLayout();
            this.readerSubMenuPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // gpiLabel
            // 
            this.gpiLabel.AutoSize = true;
            this.gpiLabel.Location = new System.Drawing.Point(6, 27);
            this.gpiLabel.Name = "gpiLabel";
            this.gpiLabel.Size = new System.Drawing.Size(68, 13);
            this.gpiLabel.TabIndex = 5;
            this.gpiLabel.Text = "Red For Low";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.GreenYellow;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Location = new System.Drawing.Point(80, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 15);
            this.label2.TabIndex = 10;
            this.label2.Text = "  ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Red;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Location = new System.Drawing.Point(114, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 15);
            this.label3.TabIndex = 11;
            this.label3.Text = "  ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.GreenYellow;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Location = new System.Drawing.Point(150, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(15, 15);
            this.label4.TabIndex = 12;
            this.label4.Text = "  ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Red;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Location = new System.Drawing.Point(185, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 15);
            this.label5.TabIndex = 13;
            this.label5.Text = "  ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Red;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Location = new System.Drawing.Point(321, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 15);
            this.label6.TabIndex = 17;
            this.label6.Text = "  ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.GreenYellow;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Location = new System.Drawing.Point(288, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(15, 15);
            this.label7.TabIndex = 16;
            this.label7.Text = "  ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Red;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.Location = new System.Drawing.Point(254, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(15, 15);
            this.label8.TabIndex = 15;
            this.label8.Text = "  ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.GreenYellow;
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label9.Location = new System.Drawing.Point(220, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(15, 15);
            this.label9.TabIndex = 14;
            this.label9.Text = "  ";
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.functionCallStatusLabel,
            this.connectionStatusLabel,
            this.connectionStatus});
            this.statusStrip.Location = new System.Drawing.Point(0, 600);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusStrip.Size = new System.Drawing.Size(1010, 25);
            this.statusStrip.TabIndex = 19;
            this.statusStrip.Text = "statusStrip";
            // 
            // functionCallStatusLabel
            // 
            this.functionCallStatusLabel.AutoSize = false;
            this.functionCallStatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.functionCallStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.functionCallStatusLabel.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.functionCallStatusLabel.Margin = new System.Windows.Forms.Padding(2, 3, 0, 2);
            this.functionCallStatusLabel.Name = "functionCallStatusLabel";
            this.functionCallStatusLabel.Size = new System.Drawing.Size(912, 20);
            this.functionCallStatusLabel.Spring = true;
            this.functionCallStatusLabel.Text = "Ready";
            this.functionCallStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // connectionStatusLabel
            // 
            this.connectionStatusLabel.Name = "connectionStatusLabel";
            this.connectionStatusLabel.Size = new System.Drawing.Size(0, 20);
            // 
            // connectionStatus
            // 
            this.connectionStatus.AutoSize = false;
            this.connectionStatus.BackgroundImage = global::CS_RFID3_Host_Sample2.Properties.Resources.disconnected;
            this.connectionStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.connectionStatus.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.connectionStatus.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.connectionStatus.Name = "connectionStatus";
            this.connectionStatus.Size = new System.Drawing.Size(50, 20);
            this.connectionStatus.Text = "Disconnected";
            // 
            // gpiStateGB
            // 
            this.gpiStateGB.Controls.Add(this.gpiNumberLabel);
            this.gpiStateGB.Controls.Add(this.label6);
            this.gpiStateGB.Controls.Add(this.label7);
            this.gpiStateGB.Controls.Add(this.label8);
            this.gpiStateGB.Controls.Add(this.label9);
            this.gpiStateGB.Controls.Add(this.label5);
            this.gpiStateGB.Controls.Add(this.label4);
            this.gpiStateGB.Controls.Add(this.label3);
            this.gpiStateGB.Controls.Add(this.label2);
            this.gpiStateGB.Controls.Add(this.gpiLabel);
            this.gpiStateGB.Location = new System.Drawing.Point(12, 331);
            this.gpiStateGB.Name = "gpiStateGB";
            this.gpiStateGB.Size = new System.Drawing.Size(347, 54);
            this.gpiStateGB.TabIndex = 20;
            this.gpiStateGB.TabStop = false;
            this.gpiStateGB.Text = "GPI State";
            // 
            // gpiNumberLabel
            // 
            this.gpiNumberLabel.AutoSize = true;
            this.gpiNumberLabel.Location = new System.Drawing.Point(77, 38);
            this.gpiNumberLabel.Name = "gpiNumberLabel";
            this.gpiNumberLabel.Size = new System.Drawing.Size(259, 13);
            this.gpiNumberLabel.TabIndex = 18;
            this.gpiNumberLabel.Text = " 1          2          3         4          5          6         7         8";
            // 
            // transmitPowerGB
            // 
            this.transmitPowerGB.Controls.Add(this.label12);
            this.transmitPowerGB.Controls.Add(this.label10);
            this.transmitPowerGB.Controls.Add(this.label11);
            this.transmitPowerGB.Controls.Add(this.hScrollBar1);
            this.transmitPowerGB.Location = new System.Drawing.Point(11, 331);
            this.transmitPowerGB.Name = "transmitPowerGB";
            this.transmitPowerGB.Size = new System.Drawing.Size(347, 53);
            this.transmitPowerGB.TabIndex = 23;
            this.transmitPowerGB.TabStop = false;
            this.transmitPowerGB.Text = "Transmit Power (dbm)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(296, 37);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(31, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "2920";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 37);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "1620";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(114, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "1620";
            // 
            // hScrollBar1
            // 
            this.hScrollBar1.Location = new System.Drawing.Point(13, 16);
            this.hScrollBar1.Maximum = 2920;
            this.hScrollBar1.Minimum = 1620;
            this.hScrollBar1.Name = "hScrollBar1";
            this.hScrollBar1.Size = new System.Drawing.Size(314, 19);
            this.hScrollBar1.TabIndex = 0;
            this.hScrollBar1.Value = 1620;
            // 
            // dataContextMenuStrip
            // 
            this.dataContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tagDataToolStripMenuItem,
            this.readDataContextMenuItem,
            this.writeDataContextMenuItem,
            this.lockDataContextMenuItem,
            this.killDataContextMenuItem,
            this.blockWriteDataContextMenuItem,
            this.blockEraseDataContextMenuItem,
            this.toolStripMenuItem1,
            this.locateTagToolStripMenuItem});
            this.dataContextMenuStrip.Name = "dataContextMenuStrip";
            this.dataContextMenuStrip.Size = new System.Drawing.Size(135, 186);
            // 
            // tagDataToolStripMenuItem
            // 
            this.tagDataToolStripMenuItem.Name = "tagDataToolStripMenuItem";
            this.tagDataToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.tagDataToolStripMenuItem.Text = "Tag Data";
            // 
            // readDataContextMenuItem
            // 
            this.readDataContextMenuItem.Name = "readDataContextMenuItem";
            this.readDataContextMenuItem.Size = new System.Drawing.Size(134, 22);
            this.readDataContextMenuItem.Text = "Read";
            // 
            // writeDataContextMenuItem
            // 
            this.writeDataContextMenuItem.Name = "writeDataContextMenuItem";
            this.writeDataContextMenuItem.Size = new System.Drawing.Size(134, 22);
            this.writeDataContextMenuItem.Text = "Write";
            // 
            // lockDataContextMenuItem
            // 
            this.lockDataContextMenuItem.Name = "lockDataContextMenuItem";
            this.lockDataContextMenuItem.Size = new System.Drawing.Size(134, 22);
            this.lockDataContextMenuItem.Text = "Lock";
            // 
            // killDataContextMenuItem
            // 
            this.killDataContextMenuItem.Name = "killDataContextMenuItem";
            this.killDataContextMenuItem.Size = new System.Drawing.Size(134, 22);
            this.killDataContextMenuItem.Text = "Kill";
            // 
            // blockWriteDataContextMenuItem
            // 
            this.blockWriteDataContextMenuItem.Name = "blockWriteDataContextMenuItem";
            this.blockWriteDataContextMenuItem.Size = new System.Drawing.Size(134, 22);
            this.blockWriteDataContextMenuItem.Text = "Block Write";
            // 
            // blockEraseDataContextMenuItem
            // 
            this.blockEraseDataContextMenuItem.Name = "blockEraseDataContextMenuItem";
            this.blockEraseDataContextMenuItem.Size = new System.Drawing.Size(134, 22);
            this.blockEraseDataContextMenuItem.Text = "Block Erase";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(131, 6);
            // 
            // locateTagToolStripMenuItem
            // 
            this.locateTagToolStripMenuItem.Name = "locateTagToolStripMenuItem";
            this.locateTagToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.locateTagToolStripMenuItem.Text = "Locate Tag";
            // 
            // connectBackgroundWorker
            // 
            this.connectBackgroundWorker.WorkerReportsProgress = true;
            this.connectBackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.connectBackgroundWorker_DoWork);
            this.connectBackgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.connectBackgroundWorker_ProgressChanged);
            this.connectBackgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.connectBackgroundWorker_RunWorkerCompleted);
            // 
            // sideMenuPanel
            // 
            this.sideMenuPanel.AutoScroll = true;
            this.sideMenuPanel.BackColor = System.Drawing.Color.Black;
            this.sideMenuPanel.Controls.Add(this.btnLogout);
            this.sideMenuPanel.Controls.Add(this.btnAbout);
            this.sideMenuPanel.Controls.Add(this.subMenuOperationsPanel);
            this.sideMenuPanel.Controls.Add(this.btnOperations);
            this.sideMenuPanel.Controls.Add(this.subMenuConfigPanel);
            this.sideMenuPanel.Controls.Add(this.btnMasterData);
            this.sideMenuPanel.Controls.Add(this.readerSubMenuPanel);
            this.sideMenuPanel.Controls.Add(this.btnReader);
            this.sideMenuPanel.Controls.Add(this.pictureBox1);
            this.sideMenuPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.sideMenuPanel.Location = new System.Drawing.Point(0, 0);
            this.sideMenuPanel.Name = "sideMenuPanel";
            this.sideMenuPanel.Size = new System.Drawing.Size(239, 600);
            this.sideMenuPanel.TabIndex = 22;
            // 
            // btnLogout
            // 
            this.btnLogout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            this.btnLogout.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnLogout.FlatAppearance.BorderSize = 0;
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.ForeColor = System.Drawing.Color.White;
            this.btnLogout.Location = new System.Drawing.Point(0, 570);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnLogout.Size = new System.Drawing.Size(239, 30);
            this.btnLogout.TabIndex = 10;
            this.btnLogout.Text = "Logout";
            this.btnLogout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLogout.UseVisualStyleBackColor = false;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // btnAbout
            // 
            this.btnAbout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            this.btnAbout.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAbout.FlatAppearance.BorderSize = 0;
            this.btnAbout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAbout.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAbout.ForeColor = System.Drawing.Color.White;
            this.btnAbout.Location = new System.Drawing.Point(0, 411);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnAbout.Size = new System.Drawing.Size(239, 30);
            this.btnAbout.TabIndex = 9;
            this.btnAbout.Text = "About";
            this.btnAbout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAbout.UseVisualStyleBackColor = false;
            // 
            // subMenuOperationsPanel
            // 
            this.subMenuOperationsPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.subMenuOperationsPanel.Controls.Add(this.button5);
            this.subMenuOperationsPanel.Controls.Add(this.button4);
            this.subMenuOperationsPanel.Controls.Add(this.button3);
            this.subMenuOperationsPanel.Controls.Add(this.button1);
            this.subMenuOperationsPanel.Controls.Add(this.button2);
            this.subMenuOperationsPanel.Controls.Add(this.btnNewRegisteredLinen);
            this.subMenuOperationsPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.subMenuOperationsPanel.Location = new System.Drawing.Point(0, 266);
            this.subMenuOperationsPanel.Name = "subMenuOperationsPanel";
            this.subMenuOperationsPanel.Size = new System.Drawing.Size(239, 145);
            this.subMenuOperationsPanel.TabIndex = 7;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.button5.Dock = System.Windows.Forms.DockStyle.Top;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.button5.Location = new System.Drawing.Point(0, 125);
            this.button5.Name = "button5";
            this.button5.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.button5.Size = new System.Drawing.Size(239, 25);
            this.button5.TabIndex = 49;
            this.button5.Text = "     Distribusi Bersih";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.button4.Dock = System.Windows.Forms.DockStyle.Top;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.button4.Location = new System.Drawing.Point(0, 100);
            this.button4.Name = "button4";
            this.button4.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.button4.Size = new System.Drawing.Size(239, 25);
            this.button4.TabIndex = 48;
            this.button4.Text = "     Linen Belum Terscan";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.button3.Dock = System.Windows.Forms.DockStyle.Top;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.button3.Location = new System.Drawing.Point(0, 75);
            this.button3.Name = "button3";
            this.button3.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.button3.Size = new System.Drawing.Size(239, 25);
            this.button3.TabIndex = 47;
            this.button3.Text = "     Selisih Linen";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.UseVisualStyleBackColor = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.button1.Location = new System.Drawing.Point(0, 50);
            this.button1.Name = "button1";
            this.button1.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.button1.Size = new System.Drawing.Size(239, 25);
            this.button1.TabIndex = 46;
            this.button1.Text = "     Linen Kotor";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.button2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.button2.Location = new System.Drawing.Point(0, 25);
            this.button2.Name = "button2";
            this.button2.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.button2.Size = new System.Drawing.Size(239, 25);
            this.button2.TabIndex = 45;
            this.button2.Text = "     Registered Existing Linen";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // btnNewRegisteredLinen
            // 
            this.btnNewRegisteredLinen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.btnNewRegisteredLinen.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnNewRegisteredLinen.FlatAppearance.BorderSize = 0;
            this.btnNewRegisteredLinen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewRegisteredLinen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewRegisteredLinen.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnNewRegisteredLinen.Location = new System.Drawing.Point(0, 0);
            this.btnNewRegisteredLinen.Name = "btnNewRegisteredLinen";
            this.btnNewRegisteredLinen.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnNewRegisteredLinen.Size = new System.Drawing.Size(239, 25);
            this.btnNewRegisteredLinen.TabIndex = 44;
            this.btnNewRegisteredLinen.Text = "     Registered New Linen";
            this.btnNewRegisteredLinen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNewRegisteredLinen.UseVisualStyleBackColor = false;
            // 
            // btnOperations
            // 
            this.btnOperations.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            this.btnOperations.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnOperations.FlatAppearance.BorderSize = 0;
            this.btnOperations.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOperations.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOperations.ForeColor = System.Drawing.Color.White;
            this.btnOperations.Image = ((System.Drawing.Image)(resources.GetObject("btnOperations.Image")));
            this.btnOperations.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOperations.Location = new System.Drawing.Point(0, 236);
            this.btnOperations.Name = "btnOperations";
            this.btnOperations.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnOperations.Size = new System.Drawing.Size(239, 30);
            this.btnOperations.TabIndex = 6;
            this.btnOperations.Text = "      Reports";
            this.btnOperations.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOperations.UseVisualStyleBackColor = false;
            this.btnOperations.Click += new System.EventHandler(this.btnOperations_Click_1);
            // 
            // subMenuConfigPanel
            // 
            this.subMenuConfigPanel.Controls.Add(this.btnGrouping);
            this.subMenuConfigPanel.Controls.Add(this.btnLinenRegister);
            this.subMenuConfigPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.subMenuConfigPanel.Location = new System.Drawing.Point(0, 189);
            this.subMenuConfigPanel.Name = "subMenuConfigPanel";
            this.subMenuConfigPanel.Size = new System.Drawing.Size(239, 47);
            this.subMenuConfigPanel.TabIndex = 5;
            // 
            // btnGrouping
            // 
            this.btnGrouping.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.btnGrouping.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnGrouping.FlatAppearance.BorderSize = 0;
            this.btnGrouping.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGrouping.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrouping.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnGrouping.Location = new System.Drawing.Point(0, 25);
            this.btnGrouping.Name = "btnGrouping";
            this.btnGrouping.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnGrouping.Size = new System.Drawing.Size(239, 25);
            this.btnGrouping.TabIndex = 3;
            this.btnGrouping.Text = "     Linen Grouping";
            this.btnGrouping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGrouping.UseVisualStyleBackColor = false;
            this.btnGrouping.Click += new System.EventHandler(this.btnGrouping_Click);
            // 
            // btnLinenRegister
            // 
            this.btnLinenRegister.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.btnLinenRegister.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLinenRegister.FlatAppearance.BorderSize = 0;
            this.btnLinenRegister.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLinenRegister.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLinenRegister.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnLinenRegister.Location = new System.Drawing.Point(0, 0);
            this.btnLinenRegister.Name = "btnLinenRegister";
            this.btnLinenRegister.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnLinenRegister.Size = new System.Drawing.Size(239, 25);
            this.btnLinenRegister.TabIndex = 1;
            this.btnLinenRegister.Text = "     Linen Register";
            this.btnLinenRegister.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLinenRegister.UseVisualStyleBackColor = false;
            this.btnLinenRegister.Click += new System.EventHandler(this.btnLinenRegister_Click);
            // 
            // btnMasterData
            // 
            this.btnMasterData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            this.btnMasterData.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMasterData.FlatAppearance.BorderSize = 0;
            this.btnMasterData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMasterData.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMasterData.ForeColor = System.Drawing.Color.White;
            this.btnMasterData.Image = ((System.Drawing.Image)(resources.GetObject("btnMasterData.Image")));
            this.btnMasterData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMasterData.Location = new System.Drawing.Point(0, 159);
            this.btnMasterData.Name = "btnMasterData";
            this.btnMasterData.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnMasterData.Size = new System.Drawing.Size(239, 30);
            this.btnMasterData.TabIndex = 4;
            this.btnMasterData.Text = "      Master Data";
            this.btnMasterData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMasterData.UseVisualStyleBackColor = false;
            this.btnMasterData.Click += new System.EventHandler(this.btnMasterData_Click);
            // 
            // readerSubMenuPanel
            // 
            this.readerSubMenuPanel.Controls.Add(this.btnAntenna);
            this.readerSubMenuPanel.Controls.Add(this.btnConnection);
            this.readerSubMenuPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.readerSubMenuPanel.Location = new System.Drawing.Point(0, 112);
            this.readerSubMenuPanel.Name = "readerSubMenuPanel";
            this.readerSubMenuPanel.Size = new System.Drawing.Size(239, 47);
            this.readerSubMenuPanel.TabIndex = 2;
            // 
            // btnAntenna
            // 
            this.btnAntenna.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.btnAntenna.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAntenna.FlatAppearance.BorderSize = 0;
            this.btnAntenna.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAntenna.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAntenna.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnAntenna.Location = new System.Drawing.Point(0, 25);
            this.btnAntenna.Name = "btnAntenna";
            this.btnAntenna.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnAntenna.Size = new System.Drawing.Size(239, 25);
            this.btnAntenna.TabIndex = 3;
            this.btnAntenna.Text = "     Antenna";
            this.btnAntenna.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAntenna.UseVisualStyleBackColor = false;
            this.btnAntenna.Click += new System.EventHandler(this.btnAntenna_Click);
            // 
            // btnConnection
            // 
            this.btnConnection.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(192)))));
            this.btnConnection.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnConnection.FlatAppearance.BorderSize = 0;
            this.btnConnection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnection.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnConnection.Location = new System.Drawing.Point(0, 0);
            this.btnConnection.Name = "btnConnection";
            this.btnConnection.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnConnection.Size = new System.Drawing.Size(239, 25);
            this.btnConnection.TabIndex = 0;
            this.btnConnection.Text = "     Connection";
            this.btnConnection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConnection.UseVisualStyleBackColor = false;
            this.btnConnection.Click += new System.EventHandler(this.btnConnection_Click);
            // 
            // btnReader
            // 
            this.btnReader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            this.btnReader.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnReader.FlatAppearance.BorderSize = 0;
            this.btnReader.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReader.ForeColor = System.Drawing.Color.White;
            this.btnReader.Image = ((System.Drawing.Image)(resources.GetObject("btnReader.Image")));
            this.btnReader.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReader.Location = new System.Drawing.Point(0, 82);
            this.btnReader.Name = "btnReader";
            this.btnReader.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnReader.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnReader.Size = new System.Drawing.Size(239, 30);
            this.btnReader.TabIndex = 1;
            this.btnReader.Text = "  Reader";
            this.btnReader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReader.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReader.UseVisualStyleBackColor = false;
            this.btnReader.Click += new System.EventHandler(this.btnReader_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(239, 82);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // mainPanel
            // 
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(239, 0);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(771, 600);
            this.mainPanel.TabIndex = 24;
            // 
            // AppForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1010, 625);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.sideMenuPanel);
            this.Controls.Add(this.statusStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(16, 250);
            this.Name = "AppForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "LTS";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AppForm_FormClosing);
            this.Load += new System.EventHandler(this.AppForm_Load);
            this.ClientSizeChanged += new System.EventHandler(this.AppForm_ClientSizeChanged);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.gpiStateGB.ResumeLayout(false);
            this.gpiStateGB.PerformLayout();
            this.transmitPowerGB.ResumeLayout(false);
            this.transmitPowerGB.PerformLayout();
            this.dataContextMenuStrip.ResumeLayout(false);
            this.sideMenuPanel.ResumeLayout(false);
            this.subMenuOperationsPanel.ResumeLayout(false);
            this.subMenuConfigPanel.ResumeLayout(false);
            this.readerSubMenuPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label gpiLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.StatusStrip statusStrip;
        internal System.Windows.Forms.ToolStripMenuItem tagDataToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem readDataContextMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem writeDataContextMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem lockDataContextMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem killDataContextMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem blockWriteDataContextMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem blockEraseDataContextMenuItem;
        private System.Windows.Forms.GroupBox gpiStateGB;
        private System.Windows.Forms.Label gpiNumberLabel;
        private System.Windows.Forms.GroupBox transmitPowerGB;
        private System.Windows.Forms.ToolStripStatusLabel connectionStatusLabel;
        private System.Windows.Forms.HScrollBar hScrollBar1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ContextMenuStrip dataContextMenuStrip;
        private System.Windows.Forms.ToolStripStatusLabel connectionStatus;
        internal System.ComponentModel.BackgroundWorker connectBackgroundWorker;
        internal System.ComponentModel.BackgroundWorker accessBackgroundWorker;
        internal System.Windows.Forms.ToolStripStatusLabel functionCallStatusLabel;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem locateTagToolStripMenuItem;
        private System.Windows.Forms.Panel sideMenuPanel;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Button btnAbout;
        private System.Windows.Forms.Panel subMenuOperationsPanel;
        private System.Windows.Forms.Button btnOperations;
        private System.Windows.Forms.Panel subMenuConfigPanel;
        private System.Windows.Forms.Button btnGrouping;
        private System.Windows.Forms.Button btnLinenRegister;
        private System.Windows.Forms.Button btnMasterData;
        private System.Windows.Forms.Panel readerSubMenuPanel;
        private System.Windows.Forms.Button btnConnection;
        private System.Windows.Forms.Button btnReader;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.Button btnAntenna;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnNewRegisteredLinen;
    }
}